SET FOREIGN_KEY_CHECKS=0;


DROP TABLE IF EXISTS `ke_action`;
CREATE TABLE `ke_action` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` char(30) NOT NULL DEFAULT '' COMMENT '行为唯一标识',
  `title` char(80) NOT NULL DEFAULT '' COMMENT '行为说明',
  `remark` char(140) NOT NULL DEFAULT '' COMMENT '行为描述',
  `rule` text NULL  COMMENT '行为规则',
  `log` text NULL  COMMENT '日志规则',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '类型',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='系统行为表';


INSERT INTO `ke_action` VALUES ('1', 'user_login', '用户登录', '积分+10，每天一次', 'table:member|field:score|condition:uid={$self} AND status>-1|rule:score+10|cycle:24|max:1;', '[user|get_nickname]在[time|time_format]登录了后台', '1', '1', '1387181220');
INSERT INTO `ke_action` VALUES ('2', 'add_article', '发布文章', '积分+5，每天上限5次', 'table:member|field:score|condition:uid={$self}|rule:score+5|cycle:24|max:5', '', '2', '0', '1380173180');
INSERT INTO `ke_action` VALUES ('3', 'review', '评论', '评论积分+1，无限制', 'table:member|field:score|condition:uid={$self}|rule:score+1', '', '2', '1', '1383285646');
INSERT INTO `ke_action` VALUES ('4', 'add_document', '发表文档', '积分+10，每天上限5次', 'table:member|field:score|condition:uid={$self}|rule:score+10|cycle:24|max:5', '[user|get_nickname]在[time|time_format]发表了一篇文章。\r\n表[model]，记录编号[record]。', '2', '0', '1386139726');
INSERT INTO `ke_action` VALUES ('5', 'add_document_topic', '发表讨论', '积分+5，每天上限10次', 'table:member|field:score|condition:uid={$self}|rule:score+5|cycle:24|max:10', '', '2', '0', '1383285551');
INSERT INTO `ke_action` VALUES ('6', 'update_config', '更新配置', '新增或修改或删除配置', '', '', '1', '1', '1383294988');
INSERT INTO `ke_action` VALUES ('7', 'update_model', '更新模型', '新增或修改模型', '', '', '1', '1', '1383295057');
INSERT INTO `ke_action` VALUES ('8', 'update_attribute', '更新属性', '新增或更新或删除属性', '', '', '1', '1', '1383295963');
INSERT INTO `ke_action` VALUES ('9', 'update_channel', '更新导航', '新增或修改或删除导航', '', '', '1', '1', '1383296301');
INSERT INTO `ke_action` VALUES ('10', 'update_menu', '更新菜单', '新增或修改或删除菜单', '', '', '1', '1', '1383296392');
INSERT INTO `ke_action` VALUES ('11', 'update_category', '更新分类', '新增或修改或删除分类', '', '', '1', '1', '1383296765');


DROP TABLE IF EXISTS `ke_action_log`;
CREATE TABLE `ke_action_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `action_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '行为id',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '执行用户id',
  `action_ip` bigint(20) NOT NULL COMMENT '执行行为者ip',
  `model` varchar(50) NOT NULL DEFAULT '' COMMENT '触发行为的表',
  `record_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '触发行为的数据id',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '日志备注',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '执行行为的时间',
  PRIMARY KEY (`id`),
  KEY `action_ip_ix` (`action_ip`),
  KEY `action_id_ix` (`action_id`),
  KEY `user_id_ix` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='行为日志表';


DROP TABLE IF EXISTS `ke_ad`;
CREATE TABLE `ke_ad` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(255) NOT NULL,
  `pic_url` char(255) NOT NULL DEFAULT '',
  `link` char(255) NOT NULL DEFAULT '#',
  `status` int(1) NOT NULL,
  `create_time` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

INSERT INTO `ke_ad` VALUES ('7', '测试广告3', '/Uploads/Ad/2015-09-02/55e6ae67f3f70.jpg', '#', '1', '1439528494');
INSERT INTO `ke_ad` VALUES ('6', '测试广告2', '/Uploads/Ad/2015-09-02/55e6aea037682.jpg', '#', '1', '1439528459');
INSERT INTO `ke_ad` VALUES ('5', '测试广告1', '/Uploads/Ad/2015-09-02/55e6ae75617c2.jpg', '#', '1', '1439527188');

DROP TABLE IF EXISTS `ke_addons`;
CREATE TABLE `ke_addons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(40) NOT NULL COMMENT '插件名或标识',
  `title` varchar(20) NOT NULL DEFAULT '' COMMENT '中文名',
  `description` text COMMENT '插件描述',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `config` text COMMENT '配置',
  `author` varchar(40) DEFAULT '' COMMENT '作者',
  `version` varchar(20) DEFAULT '' COMMENT '版本号',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '安装时间',
  `has_adminlist` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否有后台列表',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='插件表';


INSERT INTO `ke_addons` VALUES ('1', 'EditorForAdmin', '后台编辑器', '用于增强整站长文本的输入和显示', '1', '{\"editor_type\":\"2\",\"editor_wysiwyg\":\"1\",\"editor_height\":\"500px\",\"editor_resize_type\":\"1\"}', 'thinkphp', '0.1', '1383126253', '0');
INSERT INTO `ke_addons` VALUES ('2', 'SiteStat', '站点统计信息', '统计站点的基础信息', '1', '{\"title\":\"\\u7cfb\\u7edf\\u4fe1\\u606f\",\"width\":\"1\",\"display\":\"1\",\"status\":\"0\"}', 'thinkphp', '0.1', '1379512015', '0');

INSERT INTO `ke_addons` VALUES ('3', 'SystemInfo', '系统环境信息', '用于显示一些服务器的信息', '1', '{\"title\":\"\\u7cfb\\u7edf\\u4fe1\\u606f\",\"width\":\"2\",\"display\":\"1\"}', 'thinkphp', '0.1', '1379512036', '0');
INSERT INTO `ke_addons` VALUES ('4', 'Editor', '前台编辑器', '用于增强整站长文本的输入和显示', '1', '{\"editor_type\":\"2\",\"editor_wysiwyg\":\"1\",\"editor_height\":\"300px\",\"editor_resize_type\":\"1\"}', 'thinkphp', '0.1', '1379830910', '0');
INSERT INTO `ke_addons` VALUES ('5', 'Attachment', '附件', '用于文档模型上传附件', '1', 'null', 'thinkphp', '0.1', '1379842319', '1');
INSERT INTO `ke_addons` VALUES ('6', 'SocialComment', '通用社交化评论', '集成了各种社交化评论插件，轻松集成到系统中。', '1', '{\"comment_type\":\"1\",\"comment_uid_youyan\":\"\",\"comment_short_name_duoshuo\":\"\",\"comment_data_list_duoshuo\":\"\"}', 'thinkphp', '0.1', '1380273962', '0');


DROP TABLE IF EXISTS `ke_attachment`;
CREATE TABLE `ke_attachment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `title` char(30) NOT NULL DEFAULT '' COMMENT '附件显示名',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '附件类型',
  `source` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '资源ID',
  `record_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '关联记录ID',
  `download` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '下载次数',
  `size` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '附件大小',
  `dir` int(12) unsigned NOT NULL DEFAULT '0' COMMENT '上级目录ID',
  `sort` int(8) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `idx_record_status` (`record_id`,`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='附件表';


DROP TABLE IF EXISTS `ke_attribute`;
CREATE TABLE `ke_attribute` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '字段名',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '字段注释',
  `field` varchar(100) NOT NULL DEFAULT '' COMMENT '字段定义',
  `type` varchar(20) NOT NULL DEFAULT '' COMMENT '数据类型',
  `value` varchar(100) NOT NULL DEFAULT '' COMMENT '字段默认值',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `is_show` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否显示',
  `extra` varchar(255) NOT NULL DEFAULT '' COMMENT '参数',
  `model_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '模型id',
  `is_must` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否必填',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `validate_rule` varchar(255) NOT NULL DEFAULT '',
  `validate_time` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `error_info` varchar(100) NOT NULL DEFAULT '',
  `validate_type` varchar(25) NOT NULL DEFAULT '',
  `auto_rule` varchar(100) NOT NULL DEFAULT '',
  `auto_time` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `auto_type` varchar(25) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `model_id` (`model_id`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COMMENT='模型属性表';


INSERT INTO `ke_attribute` VALUES ('1', 'uid', '用户ID', 'int(10) unsigned NOT NULL ', 'num', '0', '', '0', '', '1', '0', '1', '1384508362', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `ke_attribute` VALUES ('2', 'name', '标识', 'char(40) NOT NULL ', 'string', '', '同一根节点下标识不重复', '1', '', '1', '0', '1', '1383894743', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `ke_attribute` VALUES ('3', 'title', '标题', 'char(80) NOT NULL ', 'string', '', '文档标题', '1', '', '1', '0', '1', '1383894778', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `ke_attribute` VALUES ('4', 'category_id', '所属分类', 'int(10) unsigned NOT NULL ', 'string', '', '', '0', '', '1', '0', '1', '1384508336', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `ke_attribute` VALUES ('5', 'description', '描述', 'char(140) NOT NULL ', 'textarea', '', '', '1', '', '1', '0', '1', '1383894927', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `ke_attribute` VALUES ('6', 'root', '根节点', 'int(10) unsigned NOT NULL ', 'num', '0', '该文档的顶级文档编号', '0', '', '1', '0', '1', '1384508323', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `ke_attribute` VALUES ('7', 'pid', '所属ID', 'int(10) unsigned NOT NULL ', 'num', '0', '父文档编号', '0', '', '1', '0', '1', '1384508543', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `ke_attribute` VALUES ('8', 'model_id', '内容模型ID', 'tinyint(3) unsigned NOT NULL ', 'num', '0', '该文档所对应的模型', '0', '', '1', '0', '1', '1384508350', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `ke_attribute` VALUES ('9', 'type', '内容类型', 'tinyint(3) unsigned NOT NULL ', 'select', '2', '', '1', '1:目录\r\n2:主题\r\n3:段落', '1', '0', '1', '1384511157', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `ke_attribute` VALUES ('10', 'position', '推荐位', 'smallint(5) unsigned NOT NULL ', 'checkbox', '0', '多个推荐则将其推荐值相加', '1', '[DOCUMENT_POSITION]', '1', '0', '1', '1383895640', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `ke_attribute` VALUES ('11', 'link_id', '外链', 'int(10) unsigned NOT NULL ', 'num', '0', '0-非外链，大于0-外链ID,需要函数进行链接与编号的转换', '1', '', '1', '0', '1', '1383895757', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `ke_attribute` VALUES ('12', 'cover_id', '封面', 'int(10) unsigned NOT NULL ', 'picture', '0', '0-无封面，大于0-封面图片ID，需要函数处理', '1', '', '1', '0', '1', '1384147827', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `ke_attribute` VALUES ('13', 'display', '可见性', 'tinyint(3) unsigned NOT NULL ', 'radio', '1', '', '1', '0:不可见\r\n1:所有人可见', '1', '0', '1', '1386662271', '1383891233', '', '0', '', 'regex', '', '0', 'function');
INSERT INTO `ke_attribute` VALUES ('14', 'deadline', '截至时间', 'int(10) unsigned NOT NULL ', 'datetime', '0', '0-永久有效', '1', '', '1', '0', '1', '1387163248', '1383891233', '', '0', '', 'regex', '', '0', 'function');
INSERT INTO `ke_attribute` VALUES ('15', 'attach', '附件数量', 'tinyint(3) unsigned NOT NULL ', 'num', '0', '', '0', '', '1', '0', '1', '1387260355', '1383891233', '', '0', '', 'regex', '', '0', 'function');
INSERT INTO `ke_attribute` VALUES ('16', 'view', '浏览量', 'int(10) unsigned NOT NULL ', 'num', '0', '', '1', '', '1', '0', '1', '1383895835', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `ke_attribute` VALUES ('17', 'comment', '评论数', 'int(10) unsigned NOT NULL ', 'num', '0', '', '1', '', '1', '0', '1', '1383895846', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `ke_attribute` VALUES ('18', 'extend', '扩展统计字段', 'int(10) unsigned NOT NULL ', 'num', '0', '根据需求自行使用', '0', '', '1', '0', '1', '1384508264', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `ke_attribute` VALUES ('19', 'level', '优先级', 'int(10) unsigned NOT NULL ', 'num', '0', '越高排序越靠前', '1', '', '1', '0', '1', '1383895894', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `ke_attribute` VALUES ('20', 'create_time', '创建时间', 'int(10) unsigned NOT NULL ', 'datetime', '0', '', '1', '', '1', '0', '1', '1383895903', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `ke_attribute` VALUES ('21', 'update_time', '更新时间', 'int(10) unsigned NOT NULL ', 'datetime', '0', '', '0', '', '1', '0', '1', '1384508277', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `ke_attribute` VALUES ('22', 'status', '数据状态', 'tinyint(4) NOT NULL ', 'radio', '0', '', '0', '-1:删除\r\n0:禁用\r\n1:正常\r\n2:待审核\r\n3:草稿', '1', '0', '1', '1384508496', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `ke_attribute` VALUES ('23', 'parse', '内容解析类型', 'tinyint(3) unsigned NOT NULL ', 'select', '0', '', '0', '0:html\r\n1:ubb\r\n2:markdown', '2', '0', '1', '1384511049', '1383891243', '', '0', '', '', '', '0', '');
INSERT INTO `ke_attribute` VALUES ('24', 'content', '文章内容', 'text NOT NULL ', 'editor', '', '', '1', '', '2', '0', '1', '1383896225', '1383891243', '', '0', '', '', '', '0', '');
INSERT INTO `ke_attribute` VALUES ('25', 'template', '详情页显示模板', 'varchar(100) NOT NULL ', 'string', '', '参照display方法参数的定义', '1', '', '2', '0', '1', '1383896190', '1383891243', '', '0', '', '', '', '0', '');
INSERT INTO `ke_attribute` VALUES ('26', 'bookmark', '收藏数', 'int(10) unsigned NOT NULL ', 'num', '0', '', '1', '', '2', '0', '1', '1383896103', '1383891243', '', '0', '', '', '', '0', '');
INSERT INTO `ke_attribute` VALUES ('27', 'parse', '内容解析类型', 'tinyint(3) unsigned NOT NULL ', 'select', '0', '', '0', '0:html\r\n1:ubb\r\n2:markdown', '3', '0', '1', '1387260461', '1383891252', '', '0', '', 'regex', '', '0', 'function');
INSERT INTO `ke_attribute` VALUES ('28', 'content', '下载详细描述', 'text NOT NULL ', 'editor', '', '', '1', '', '3', '0', '1', '1383896438', '1383891252', '', '0', '', '', '', '0', '');
INSERT INTO `ke_attribute` VALUES ('29', 'template', '详情页显示模板', 'varchar(100) NOT NULL ', 'string', '', '', '1', '', '3', '0', '1', '1383896429', '1383891252', '', '0', '', '', '', '0', '');
INSERT INTO `ke_attribute` VALUES ('30', 'file_id', '文件ID', 'int(10) unsigned NOT NULL ', 'file', '0', '需要函数处理', '1', '', '3', '0', '1', '1383896415', '1383891252', '', '0', '', '', '', '0', '');
INSERT INTO `ke_attribute` VALUES ('31', 'download', '下载次数', 'int(10) unsigned NOT NULL ', 'num', '0', '', '1', '', '3', '0', '1', '1383896380', '1383891252', '', '0', '', '', '', '0', '');
INSERT INTO `ke_attribute` VALUES ('32', 'size', '文件大小', 'bigint(20) unsigned NOT NULL ', 'num', '0', '单位bit', '1', '', '3', '0', '1', '1383896371', '1383891252', '', '0', '', '', '', '0', '');
INSERT INTO `ke_attribute` VALUES ('34', 'tag', '标签', 'int(10) UNSIGNED NOT NULL', 'num', '', '', '1', '', '1', '0', '1', '1439630387', '1439630387', '', '3', '', 'regex', '', '3', 'function');

DROP TABLE IF EXISTS `ke_auth_extend`;
CREATE TABLE `ke_auth_extend` (
  `group_id` mediumint(10) unsigned NOT NULL COMMENT '用户id',
  `extend_id` mediumint(8) unsigned NOT NULL COMMENT '扩展表中数据的id',
  `type` tinyint(1) unsigned NOT NULL COMMENT '扩展类型标识 1:栏目分类权限;2:模型权限',
  UNIQUE KEY `group_extend_type` (`group_id`,`extend_id`,`type`),
  KEY `uid` (`group_id`),
  KEY `group_id` (`extend_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户组与分类的对应关系表';

INSERT INTO `ke_auth_extend` VALUES ('1', '1', '1');
INSERT INTO `ke_auth_extend` VALUES ('1', '1', '2');
INSERT INTO `ke_auth_extend` VALUES ('1', '2', '1');
INSERT INTO `ke_auth_extend` VALUES ('1', '2', '2');
INSERT INTO `ke_auth_extend` VALUES ('1', '3', '1');
INSERT INTO `ke_auth_extend` VALUES ('1', '3', '2');
INSERT INTO `ke_auth_extend` VALUES ('1', '4', '1');
INSERT INTO `ke_auth_extend` VALUES ('1', '37', '1');

DROP TABLE IF EXISTS `ke_auth_group`;
CREATE TABLE `ke_auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户组id,自增主键',
  `module` varchar(20) NOT NULL DEFAULT '' COMMENT '用户组所属模块',
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '组类型',
  `title` char(20) NOT NULL DEFAULT '' COMMENT '用户组中文名称',
  `description` varchar(80) NOT NULL DEFAULT '' COMMENT '描述信息',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '用户组状态：为1正常，为0禁用,-1为删除',
  `rules` varchar(500) NOT NULL DEFAULT '' COMMENT '用户组拥有的规则id，多个规则 , 隔开',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO `ke_auth_group` VALUES ('1', 'admin', '1', '默认用户组', '', '1', '1,2,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,79,80,81,82,83,84,86,87,88,89,90,91,92,93,94,95,96,97,100,102,103,105,106');
INSERT INTO `ke_auth_group` VALUES ('2', 'admin', '1', '测试用户', '测试用户', '1', '1,2,5,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,79,80,82,83,84,88,89,90,91,92,93,96,97,100,102,103,195');

DROP TABLE IF EXISTS `ke_auth_group_access`;
CREATE TABLE `ke_auth_group_access` (
  `uid` int(10) unsigned NOT NULL COMMENT '用户id',
  `group_id` mediumint(8) unsigned NOT NULL COMMENT '用户组id',
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `ke_auth_rule`;
CREATE TABLE `ke_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '规则id,自增主键',
  `module` varchar(20) NOT NULL COMMENT '规则所属module',
  `type` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1-url;2-主菜单',
  `name` char(80) NOT NULL DEFAULT '' COMMENT '规则唯一英文标识',
  `title` char(20) NOT NULL DEFAULT '' COMMENT '规则中文描述',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否有效(0:无效,1:有效)',
  `condition` varchar(300) NOT NULL DEFAULT '' COMMENT '规则附加条件',
  PRIMARY KEY (`id`),
  KEY `module` (`module`,`status`,`type`)
) ENGINE=MyISAM AUTO_INCREMENT=217 DEFAULT CHARSET=utf8;

INSERT INTO `ke_auth_rule` VALUES ('1', 'admin', '2', 'Admin/Index/index', '首页', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('2', 'admin', '2', 'Admin/Article/index', '内容', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('3', 'admin', '2', 'Admin/User/index', '用户', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('4', 'admin', '2', 'Admin/Addons/index', '扩展', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('5', 'admin', '2', 'Admin/Config/group', '系统', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('7', 'admin', '1', 'Admin/article/add', '新增', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('8', 'admin', '1', 'Admin/article/edit', '编辑', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('9', 'admin', '1', 'Admin/article/setStatus', '改变状态', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('10', 'admin', '1', 'Admin/article/update', '保存', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('11', 'admin', '1', 'Admin/article/autoSave', '保存草稿', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('12', 'admin', '1', 'Admin/article/move', '移动', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('13', 'admin', '1', 'Admin/article/copy', '复制', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('14', 'admin', '1', 'Admin/article/paste', '粘贴', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('15', 'admin', '1', 'Admin/article/permit', '还原', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('16', 'admin', '1', 'Admin/article/clear', '清空', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('17', 'admin', '1', 'Admin/article/examine', '审核列表', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('18', 'admin', '1', 'Admin/article/recycle', '回收站', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('19', 'admin', '1', 'Admin/User/addaction', '新增用户行为', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('20', 'admin', '1', 'Admin/User/editaction', '编辑用户行为', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('21', 'admin', '1', 'Admin/User/saveAction', '保存用户行为', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('22', 'admin', '1', 'Admin/User/setStatus', '变更行为状态', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('23', 'admin', '1', 'Admin/User/changeStatus?method=forbidUser', '禁用会员', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('24', 'admin', '1', 'Admin/User/changeStatus?method=resumeUser', '启用会员', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('25', 'admin', '1', 'Admin/User/changeStatus?method=deleteUser', '删除会员', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('26', 'admin', '1', 'Admin/User/index', '用户信息', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('27', 'admin', '1', 'Admin/User/action', '用户行为', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('28', 'admin', '1', 'Admin/AuthManager/changeStatus?method=deleteGroup', '删除', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('29', 'admin', '1', 'Admin/AuthManager/changeStatus?method=forbidGroup', '禁用', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('30', 'admin', '1', 'Admin/AuthManager/changeStatus?method=resumeGroup', '恢复', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('31', 'admin', '1', 'Admin/AuthManager/createGroup', '新增', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('32', 'admin', '1', 'Admin/AuthManager/editGroup', '编辑', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('33', 'admin', '1', 'Admin/AuthManager/writeGroup', '保存用户组', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('34', 'admin', '1', 'Admin/AuthManager/group', '授权', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('35', 'admin', '1', 'Admin/AuthManager/access', '访问授权', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('36', 'admin', '1', 'Admin/AuthManager/user', '成员授权', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('37', 'admin', '1', 'Admin/AuthManager/removeFromGroup', '解除授权', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('38', 'admin', '1', 'Admin/AuthManager/addToGroup', '保存成员授权', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('39', 'admin', '1', 'Admin/AuthManager/category', '分类授权', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('40', 'admin', '1', 'Admin/AuthManager/addToCategory', '保存分类授权', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('41', 'admin', '1', 'Admin/AuthManager/index', '权限管理', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('42', 'admin', '1', 'Admin/Addons/create', '创建', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('43', 'admin', '1', 'Admin/Addons/checkForm', '检测创建', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('44', 'admin', '1', 'Admin/Addons/preview', '预览', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('45', 'admin', '1', 'Admin/Addons/build', '快速生成插件', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('46', 'admin', '1', 'Admin/Addons/config', '设置', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('47', 'admin', '1', 'Admin/Addons/disable', '禁用', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('48', 'admin', '1', 'Admin/Addons/enable', '启用', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('49', 'admin', '1', 'Admin/Addons/install', '安装', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('50', 'admin', '1', 'Admin/Addons/uninstall', '卸载', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('51', 'admin', '1', 'Admin/Addons/saveconfig', '更新配置', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('52', 'admin', '1', 'Admin/Addons/adminList', '插件后台列表', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('53', 'admin', '1', 'Admin/Addons/execute', 'URL方式访问插件', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('54', 'admin', '1', 'Admin/Addons/index', '插件管理', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('55', 'admin', '1', 'Admin/Addons/hooks', '钩子管理', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('56', 'admin', '1', 'Admin/model/add', '新增', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('57', 'admin', '1', 'Admin/model/edit', '编辑', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('58', 'admin', '1', 'Admin/model/setStatus', '改变状态', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('59', 'admin', '1', 'Admin/model/update', '保存数据', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('60', 'admin', '1', 'Admin/Model/index', '模型管理', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('61', 'admin', '1', 'Admin/Config/edit', '编辑', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('62', 'admin', '1', 'Admin/Config/del', '删除', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('63', 'admin', '1', 'Admin/Config/add', '新增', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('64', 'admin', '1', 'Admin/Config/save', '保存', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('65', 'admin', '1', 'Admin/Config/group', '网站设置', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('66', 'admin', '1', 'Admin/Config/index', '配置管理', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('67', 'admin', '1', 'Admin/Channel/add', '新增', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('68', 'admin', '1', 'Admin/Channel/edit', '编辑', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('69', 'admin', '1', 'Admin/Channel/del', '删除', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('70', 'admin', '1', 'Admin/Channel/index', '导航管理', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('71', 'admin', '1', 'Admin/Category/edit', '编辑', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('72', 'admin', '1', 'Admin/Category/add', '新增', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('73', 'admin', '1', 'Admin/Category/remove', '删除', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('74', 'admin', '1', 'Admin/Category/index', '分类管理', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('75', 'admin', '1', 'Admin/file/upload', '上传控件', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('76', 'admin', '1', 'Admin/file/uploadPicture', '上传图片', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('77', 'admin', '1', 'Admin/file/download', '下载', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('94', 'admin', '1', 'Admin/AuthManager/modelauth', '模型授权', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('79', 'admin', '1', 'Admin/article/batchOperate', '导入', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('80', 'admin', '1', 'Admin/Database/index?type=export', '备份数据库', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('81', 'admin', '1', 'Admin/Database/index?type=import', '还原数据库', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('82', 'admin', '1', 'Admin/Database/export', '备份', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('83', 'admin', '1', 'Admin/Database/optimize', '优化表', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('84', 'admin', '1', 'Admin/Database/repair', '修复表', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('86', 'admin', '1', 'Admin/Database/import', '恢复', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('87', 'admin', '1', 'Admin/Database/del', '删除', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('88', 'admin', '1', 'Admin/User/add', '新增用户', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('89', 'admin', '1', 'Admin/Attribute/index', '属性管理', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('90', 'admin', '1', 'Admin/Attribute/add', '新增', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('91', 'admin', '1', 'Admin/Attribute/edit', '编辑', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('92', 'admin', '1', 'Admin/Attribute/setStatus', '改变状态', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('93', 'admin', '1', 'Admin/Attribute/update', '保存数据', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('95', 'admin', '1', 'Admin/AuthManager/addToModel', '保存模型授权', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('96', 'admin', '1', 'Admin/Category/move', '移动', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('97', 'admin', '1', 'Admin/Category/merge', '合并', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('98', 'admin', '1', 'Admin/Config/menu', '后台菜单管理', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('99', 'admin', '1', 'Admin/Article/mydocument', '内容', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('100', 'admin', '1', 'Admin/Menu/index', '菜单管理', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('101', 'admin', '1', 'Admin/other', '其他', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('102', 'admin', '1', 'Admin/Menu/add', '新增', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('103', 'admin', '1', 'Admin/Menu/edit', '编辑', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('104', 'admin', '1', 'Admin/Think/lists?model=article', '文章管理', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('105', 'admin', '1', 'Admin/Think/lists?model=download', '下载管理', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('106', 'admin', '1', 'Admin/Think/lists?model=config', '配置管理', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('107', 'admin', '1', 'Admin/Action/actionlog', '行为日志', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('108', 'admin', '1', 'Admin/User/updatePassword', '修改密码', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('109', 'admin', '1', 'Admin/User/updateNickname', '修改昵称', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('110', 'admin', '1', 'Admin/action/edit', '查看行为日志', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('205', 'admin', '1', 'Admin/think/add', '新增数据', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('111', 'admin', '2', 'Admin/article/index', '文档列表', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('112', 'admin', '2', 'Admin/article/add', '新增', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('113', 'admin', '2', 'Admin/article/edit', '编辑', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('114', 'admin', '2', 'Admin/article/setStatus', '改变状态', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('115', 'admin', '2', 'Admin/article/update', '保存', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('116', 'admin', '2', 'Admin/article/autoSave', '保存草稿', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('117', 'admin', '2', 'Admin/article/move', '移动', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('118', 'admin', '2', 'Admin/article/copy', '复制', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('119', 'admin', '2', 'Admin/article/paste', '粘贴', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('120', 'admin', '2', 'Admin/article/batchOperate', '导入', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('121', 'admin', '2', 'Admin/article/recycle', '回收站', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('122', 'admin', '2', 'Admin/article/permit', '还原', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('123', 'admin', '2', 'Admin/article/clear', '清空', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('124', 'admin', '2', 'Admin/User/add', '新增用户', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('125', 'admin', '2', 'Admin/User/action', '用户行为', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('126', 'admin', '2', 'Admin/User/addAction', '新增用户行为', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('127', 'admin', '2', 'Admin/User/editAction', '编辑用户行为', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('128', 'admin', '2', 'Admin/User/saveAction', '保存用户行为', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('129', 'admin', '2', 'Admin/User/setStatus', '变更行为状态', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('130', 'admin', '2', 'Admin/User/changeStatus?method=forbidUser', '禁用会员', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('131', 'admin', '2', 'Admin/User/changeStatus?method=resumeUser', '启用会员', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('132', 'admin', '2', 'Admin/User/changeStatus?method=deleteUser', '删除会员', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('133', 'admin', '2', 'Admin/AuthManager/index', '权限管理', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('134', 'admin', '2', 'Admin/AuthManager/changeStatus?method=deleteGroup', '删除', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('135', 'admin', '2', 'Admin/AuthManager/changeStatus?method=forbidGroup', '禁用', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('136', 'admin', '2', 'Admin/AuthManager/changeStatus?method=resumeGroup', '恢复', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('137', 'admin', '2', 'Admin/AuthManager/createGroup', '新增', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('138', 'admin', '2', 'Admin/AuthManager/editGroup', '编辑', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('139', 'admin', '2', 'Admin/AuthManager/writeGroup', '保存用户组', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('140', 'admin', '2', 'Admin/AuthManager/group', '授权', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('141', 'admin', '2', 'Admin/AuthManager/access', '访问授权', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('142', 'admin', '2', 'Admin/AuthManager/user', '成员授权', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('143', 'admin', '2', 'Admin/AuthManager/removeFromGroup', '解除授权', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('144', 'admin', '2', 'Admin/AuthManager/addToGroup', '保存成员授权', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('145', 'admin', '2', 'Admin/AuthManager/category', '分类授权', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('146', 'admin', '2', 'Admin/AuthManager/addToCategory', '保存分类授权', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('147', 'admin', '2', 'Admin/AuthManager/modelauth', '模型授权', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('148', 'admin', '2', 'Admin/AuthManager/addToModel', '保存模型授权', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('149', 'admin', '2', 'Admin/Addons/create', '创建', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('150', 'admin', '2', 'Admin/Addons/checkForm', '检测创建', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('151', 'admin', '2', 'Admin/Addons/preview', '预览', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('152', 'admin', '2', 'Admin/Addons/build', '快速生成插件', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('153', 'admin', '2', 'Admin/Addons/config', '设置', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('154', 'admin', '2', 'Admin/Addons/disable', '禁用', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('155', 'admin', '2', 'Admin/Addons/enable', '启用', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('156', 'admin', '2', 'Admin/Addons/install', '安装', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('157', 'admin', '2', 'Admin/Addons/uninstall', '卸载', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('158', 'admin', '2', 'Admin/Addons/saveconfig', '更新配置', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('159', 'admin', '2', 'Admin/Addons/adminList', '插件后台列表', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('160', 'admin', '2', 'Admin/Addons/execute', 'URL方式访问插件', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('161', 'admin', '2', 'Admin/Addons/hooks', '钩子管理', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('162', 'admin', '2', 'Admin/Model/index', '模型管理', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('163', 'admin', '2', 'Admin/model/add', '新增', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('164', 'admin', '2', 'Admin/model/edit', '编辑', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('165', 'admin', '2', 'Admin/model/setStatus', '改变状态', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('166', 'admin', '2', 'Admin/model/update', '保存数据', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('167', 'admin', '2', 'Admin/Attribute/index', '属性管理', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('168', 'admin', '2', 'Admin/Attribute/add', '新增', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('169', 'admin', '2', 'Admin/Attribute/edit', '编辑', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('170', 'admin', '2', 'Admin/Attribute/setStatus', '改变状态', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('171', 'admin', '2', 'Admin/Attribute/update', '保存数据', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('172', 'admin', '2', 'Admin/Config/index', '配置管理', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('173', 'admin', '2', 'Admin/Config/edit', '编辑', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('174', 'admin', '2', 'Admin/Config/del', '删除', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('175', 'admin', '2', 'Admin/Config/add', '新增', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('176', 'admin', '2', 'Admin/Config/save', '保存', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('177', 'admin', '2', 'Admin/Menu/index', '菜单管理', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('178', 'admin', '2', 'Admin/Channel/index', '导航管理', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('179', 'admin', '2', 'Admin/Channel/add', '新增', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('180', 'admin', '2', 'Admin/Channel/edit', '编辑', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('181', 'admin', '2', 'Admin/Channel/del', '删除', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('182', 'admin', '2', 'Admin/Category/index', '分类管理', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('183', 'admin', '2', 'Admin/Category/edit', '编辑', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('184', 'admin', '2', 'Admin/Category/add', '新增', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('185', 'admin', '2', 'Admin/Category/remove', '删除', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('186', 'admin', '2', 'Admin/Category/move', '移动', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('187', 'admin', '2', 'Admin/Category/merge', '合并', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('188', 'admin', '2', 'Admin/Database/index?type=export', '备份数据库', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('189', 'admin', '2', 'Admin/Database/export', '备份', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('190', 'admin', '2', 'Admin/Database/optimize', '优化表', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('191', 'admin', '2', 'Admin/Database/repair', '修复表', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('192', 'admin', '2', 'Admin/Database/index?type=import', '还原数据库', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('193', 'admin', '2', 'Admin/Database/import', '恢复', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('194', 'admin', '2', 'Admin/Database/del', '删除', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('195', 'admin', '2', 'Admin/other', '其他', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('196', 'admin', '2', 'Admin/Menu/add', '新增', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('197', 'admin', '2', 'Admin/Menu/edit', '编辑', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('198', 'admin', '2', 'Admin/Think/lists?model=article', '应用', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('199', 'admin', '2', 'Admin/Think/lists?model=download', '下载管理', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('200', 'admin', '2', 'Admin/Think/lists?model=config', '应用', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('201', 'admin', '2', 'Admin/Action/actionlog', '行为日志', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('202', 'admin', '2', 'Admin/User/updatePassword', '修改密码', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('203', 'admin', '2', 'Admin/User/updateNickname', '修改昵称', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('204', 'admin', '2', 'Admin/action/edit', '查看行为日志', '-1', '');
INSERT INTO `ke_auth_rule` VALUES ('206', 'admin', '1', 'Admin/think/edit', '编辑数据', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('207', 'admin', '1', 'Admin/Menu/import', '导入', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('208', 'admin', '1', 'Admin/Model/generate', '生成', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('209', 'admin', '1', 'Admin/Addons/addHook', '新增钩子', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('210', 'admin', '1', 'Admin/Addons/edithook', '编辑钩子', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('211', 'admin', '1', 'Admin/Article/sort', '文档排序', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('212', 'admin', '1', 'Admin/Config/sort', '排序', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('213', 'admin', '1', 'Admin/Menu/sort', '排序', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('214', 'admin', '1', 'Admin/Channel/sort', '排序', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('215', 'admin', '1', 'Admin/Category/operate/type/move', '移动', '1', '');
INSERT INTO `ke_auth_rule` VALUES ('216', 'admin', '1', 'Admin/Category/operate/type/merge', '合并', '1', '');

DROP TABLE IF EXISTS `ke_category`;
CREATE TABLE `ke_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类ID',
  `name` varchar(30) NOT NULL COMMENT '标志',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上级分类ID',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序（同级有效）',
  `list_row` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '列表每页行数',
  `meta_title` varchar(50) NOT NULL DEFAULT '' COMMENT 'SEO的网页标题',
  `keywords` varchar(255) NOT NULL DEFAULT '' COMMENT '关键字',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  `template_index` varchar(100) NOT NULL DEFAULT '' COMMENT '频道页模板',
  `template_lists` varchar(100) NOT NULL DEFAULT '' COMMENT '列表页模板',
  `template_detail` varchar(100) NOT NULL DEFAULT '' COMMENT '详情页模板',
  `template_edit` varchar(100) NOT NULL DEFAULT '' COMMENT '编辑页模板',
  `model` varchar(100) NOT NULL DEFAULT '' COMMENT '列表绑定模型',
  `model_sub` varchar(100) NOT NULL DEFAULT '' COMMENT '子文档绑定模型',
  `type` varchar(100) NOT NULL DEFAULT '' COMMENT '允许发布的内容类型',
  `link_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '外链',
  `allow_publish` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否允许发布内容',
  `display` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '可见性',
  `reply` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否允许回复',
  `check` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '发布的文章是否需要审核',
  `reply_model` varchar(100) NOT NULL DEFAULT '',
  `extend` text NULL  COMMENT '扩展设置',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '数据状态',
  `icon` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '分类图标',
  `groups` varchar(255) NOT NULL DEFAULT '' COMMENT '分组定义',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COMMENT='分类表';

INSERT INTO `ke_category` VALUES ('1', 'blog', '博客', '0', '0', '10', '', '', '', '', '', '', '', '2,3','2', '2,1', '0', '0', '1', '0', '0', '1', '', '1379474947', '1382701539', '1', '0','');
INSERT INTO `ke_category` VALUES ('2', 'default_blog', '默认分类', '1', '1', '10', '', '', '', '', '', '', '', '2,3','2', '2,1,3', '0', '1', '1', '0', '1', '1', '', '1379475028', '1386839751', '1', '0','');

DROP TABLE IF EXISTS `ke_category_goods`;
CREATE TABLE `ke_category_goods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '宝贝分类id',
  `category_name` varchar(255) NOT NULL DEFAULT '' COMMENT '宝贝分类名称',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `p_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '所属店铺',
  `goods_num` int(3) NOT NULL DEFAULT '0' COMMENT '包含宝贝数量',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '分类状态，与宝贝状态对应：0=关闭，1开启',
  `sort` int(3) unsigned NOT NULL DEFAULT '0' COMMENT '宝贝排序',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

INSERT INTO `ke_category_goods` VALUES ('1', '时尚男装', '1', '0', '0', '1', '0');
INSERT INTO `ke_category_goods` VALUES ('2', '精品女装', '1', '0', '0', '1', '0');
INSERT INTO `ke_category_goods` VALUES ('3', '生活电器', '1', '0', '0', '1', '0');
INSERT INTO `ke_category_goods` VALUES ('4', '手机数码', '1', '0', '0', '0', '0');
INSERT INTO `ke_category_goods` VALUES ('9', '手机配件', '1', '4', '0', '1', '0');
DROP TABLE IF EXISTS `ke_channel`;
CREATE TABLE `ke_channel` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '频道ID',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上级频道ID',
  `title` char(30) NOT NULL COMMENT '频道标题',
  `url` char(100) NOT NULL COMMENT '频道连接',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '导航排序',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态',
  `target` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '新窗口打开',
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO `ke_channel` VALUES ('2', '0', '时尚男装', 'Goods/cate?id=1', '2', '1379475131', '1439193763', '1', '0');
INSERT INTO `ke_channel` VALUES ('3', '0', '精品女装', 'Goods/cate?id=2', '3', '1379475154', '1439193748', '1', '0');
INSERT INTO `ke_channel` VALUES ('4', '0', '攻略', 'Home/Article/index?category=1', '6', '1439530334', '1439608763', '1', '0');

DROP TABLE IF EXISTS `ke_collect_goods`;
CREATE TABLE `ke_collect_goods` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `num_iid` varchar(20) NOT NULL DEFAULT '0',
  `title` varchar(200) NOT NULL DEFAULT '' COMMENT '宝贝名称',
  `cate_id` int(10) unsigned NOT NULL DEFAULT '0',
  `goods_type` varchar(255) NOT NULL COMMENT '商品类型',
  `price` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT '现价',
  `market_price` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT '市场价',
  `pic_url` varchar(255) NOT NULL COMMENT '图片地址',
  `item_url` varchar(255) NOT NULL DEFAULT '' COMMENT '宝贝地址',
  `click_url` text COMMENT '推广地址',
  PRIMARY KEY (`id`),
  KEY `num_iid` (`num_iid`)
) ENGINE=MyISAM AUTO_INCREMENT=91 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ke_config`;
CREATE TABLE `ke_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '配置ID',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '配置名称',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '配置类型',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '配置说明',
  `group` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '配置分组',
  `extra` varchar(255) NOT NULL DEFAULT '' COMMENT '配置值',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '配置说明',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态',
  `value` text NULL  COMMENT '配置值',
  `sort` smallint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`),
  KEY `type` (`type`),
  KEY `group` (`group`)
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

INSERT INTO `ke_config` VALUES ('1', 'WEB_SITE_TITLE', '1', '网站标题', '1', '', '网站标题前台显示标题', '1378898976', '1379235274', '1', 'Ke361 淘宝客商城', '0');
INSERT INTO `ke_config` VALUES ('2', 'WEB_SITE_DESCRIPTION', '2', '网站描述', '1', '', '网站搜索引擎描述', '1378898976', '1379235841', '1', 'Ke361 淘宝客商城', '1');
INSERT INTO `ke_config` VALUES ('3', 'WEB_SITE_KEYWORD', '2', '网站关键字', '1', '', '网站搜索引擎关键字', '1378898976', '1381390100', '1', 'Ke361 淘宝客商城', '8');
INSERT INTO `ke_config` VALUES ('4', 'WEB_SITE_CLOSE', '4', '关闭站点', '1', '0:关闭,1:开启', '站点关闭后其他用户不能访问，管理员可以正常访问', '1378898976', '1379235296', '1', '1', '1');
INSERT INTO `ke_config` VALUES ('9', 'CONFIG_TYPE_LIST', '3', '配置类型列表', '4', '', '主要用于数据解析和页面表单的生成', '1378898976', '1379235348', '1', '0:数字\r\n1:字符\r\n2:文本\r\n3:数组\r\n4:枚举', '2');
INSERT INTO `ke_config` VALUES ('10', 'WEB_SITE_ICP', '1', '网站备案号', '1', '', '设置在网站底部显示的备案号，如“沪ICP备12007941号-2', '1378900335', '1379235859', '1', '', '9');
INSERT INTO `ke_config` VALUES ('11', 'DOCUMENT_POSITION', '3', '文档推荐位', '2', '', '文档推荐位，推荐到多个位置KEY值相加即可', '1379053380', '1379235329', '1', '1:列表推荐\r\n2:频道推荐\r\n4:首页推荐', '3');
INSERT INTO `ke_config` VALUES ('12', 'DOCUMENT_DISPLAY', '3', '文档可见性', '2', '', '文章可见性仅影响前台显示，后台不收影响', '1379056370', '1379235322', '1', '0:所有人可见\r\n1:仅注册会员可见\r\n2:仅管理员可见', '4');
INSERT INTO `ke_config` VALUES ('13', 'COLOR_STYLE', '4', '后台色系', '1', 'default_color:默认\r\nblue_color:紫罗兰', '后台颜色风格', '1379122533', '1379235904', '1', 'default_color', '10');
INSERT INTO `ke_config` VALUES ('20', 'CONFIG_GROUP_LIST', '3', '配置分组', '4', '', '配置分组', '1379228036', '1384418383', '1', '1:基本\r\n2:内容\r\n3:用户\r\n4:系统\r\n5:显示\r\n6:淘客', '4');
INSERT INTO `ke_config` VALUES ('21', 'HOOKS_TYPE', '3', '钩子的类型', '4', '', '类型 1-用于扩展显示内容，2-用于扩展业务处理', '1379313397', '1379313407', '1', '1:视图\r\n2:控制器', '6');
INSERT INTO `ke_config` VALUES ('22', 'AUTH_CONFIG', '3', 'Auth配置', '4', '', '自定义Auth.class.php类配置', '1379409310', '1379409564', '1', 'AUTH_ON:1\r\nAUTH_TYPE:2', '8');
INSERT INTO `ke_config` VALUES ('23', 'OPEN_DRAFTBOX', '4', '是否开启草稿功能', '2', '0:关闭草稿功能\r\n1:开启草稿功能\r\n', '新增文章时的草稿功能配置', '1379484332', '1379484591', '1', '1', '1');
INSERT INTO `ke_config` VALUES ('24', 'DRAFT_AOTOSAVE_INTERVAL', '0', '自动保存草稿时间', '2', '', '自动保存草稿的时间间隔，单位：秒', '1379484574', '1386143323', '1', '60', '2');
INSERT INTO `ke_config` VALUES ('25', 'LIST_ROWS', '0', '后台每页记录数', '2', '', '后台数据每页显示记录数', '1379503896', '1380427745', '1', '10', '10');
INSERT INTO `ke_config` VALUES ('26', 'USER_ALLOW_REGISTER', '4', '是否允许用户注册', '3', '0:关闭注册\r\n1:允许注册', '是否开放用户注册', '1379504487', '1379504580', '1', '1', '3');
INSERT INTO `ke_config` VALUES ('27', 'CODEMIRROR_THEME', '4', '预览插件的CodeMirror主题', '4', '3024-day:3024 day\r\n3024-night:3024 night\r\nambiance:ambiance\r\nbase16-dark:base16 dark\r\nbase16-light:base16 light\r\nblackboard:blackboard\r\ncobalt:cobalt\r\neclipse:eclipse\r\nelegant:elegant\r\nerlang-dark:erlang-dark\r\nlesser-dark:lesser-dark\r\nmidnight:midnight', '详情见CodeMirror官网', '1379814385', '1384740813', '1', 'ambiance', '3');
INSERT INTO `ke_config` VALUES ('28', 'DATA_BACKUP_PATH', '1', '数据库备份根路径', '4', '', '路径必须以 / 结尾', '1381482411', '1381482411', '1', './Data/', '5');
INSERT INTO `ke_config` VALUES ('29', 'DATA_BACKUP_PART_SIZE', '0', '数据库备份卷大小', '4', '', '该值用于限制压缩后的分卷最大长度。单位：B；建议设置20M', '1381482488', '1381729564', '1', '20971520', '7');
INSERT INTO `ke_config` VALUES ('30', 'DATA_BACKUP_COMPRESS', '4', '数据库备份文件是否启用压缩', '4', '0:不压缩\r\n1:启用压缩', '压缩备份文件需要PHP环境支持gzopen,gzwrite函数', '1381713345', '1381729544', '1', '1', '9');
INSERT INTO `ke_config` VALUES ('31', 'DATA_BACKUP_COMPRESS_LEVEL', '4', '数据库备份文件压缩级别', '4', '1:普通\r\n4:一般\r\n9:最高', '数据库备份文件的压缩级别，该配置在开启压缩时生效', '1381713408', '1381713408', '1', '9', '10');
INSERT INTO `ke_config` VALUES ('32', 'DEVELOP_MODE', '4', '开启开发者模式', '4', '0:关闭\r\n1:开启', '是否开启开发者模式', '1383105995', '1383291877', '1', '1', '11');
INSERT INTO `ke_config` VALUES ('33', 'ALLOW_VISIT', '3', '不受限控制器方法', '0', '', '', '1386644047', '1386644741', '1', '0:article/draftbox\r\n1:article/mydocument\r\n2:Category/tree\r\n3:Index/verify\r\n4:file/upload\r\n5:file/download\r\n6:user/updatePassword\r\n7:user/updateNickname\r\n8:user/submitPassword\r\n9:user/submitNickname\r\n10:file/uploadpicture', '0');
INSERT INTO `ke_config` VALUES ('34', 'DENY_VISIT', '3', '超管专限控制器方法', '0', '', '仅超级管理员可访问的控制器方法', '1386644141', '1386644659', '1', '0:Addons/addhook\r\n1:Addons/edithook\r\n2:Addons/delhook\r\n3:Addons/updateHook\r\n4:Admin/getMenus\r\n5:Admin/recordList\r\n6:AuthManager/updateRules\r\n7:AuthManager/tree', '0');
INSERT INTO `ke_config` VALUES ('35', 'REPLY_LIST_ROWS', '0', '回复列表每页条数', '2', '', '', '1386645376', '1387178083', '1', '10', '0');
INSERT INTO `ke_config` VALUES ('36', 'ADMIN_ALLOW_IP', '2', '后台允许访问IP', '4', '', '多个用逗号分隔，如果不配置表示不限制IP访问', '1387165454', '1387165553', '1', '', '12');
INSERT INTO `ke_config` VALUES ('37', 'SHOW_PAGE_TRACE', '4', '是否显示页面Trace', '4', '0:关闭\r\n1:开启', '是否显示页面Trace信息', '1387165685', '1387165685', '1', '0', '1');
INSERT INTO `ke_config` VALUES ('45', 'TONGJI', '2', '统计代码', '1', '', '网站统计代码', '1437700726', '1437700726', '1', '', '0');
INSERT INTO `ke_config` VALUES ('47', 'URL_MODEL', '4', 'URL模式', '5', '0:普通模式\r\n1:PATHINFO模式\r\n2:REWRITE模式\r\n3:兼容模式', '', '1437701819', '1437701819', '1', '3', '0');
INSERT INTO `ke_config` VALUES ('48', 'HOME_DEFAULT_THEME', '4', '前台主题', '5', 'black:black\r\ndefault:default', '', '1437702225', '1439358328', '1', 'default', '1');
INSERT INTO `ke_config` VALUES ('49', 'APP_KEY', '1', '淘宝app_key', '6', '', '', '1437814132', '1437814132', '1', '23207033', '0');
INSERT INTO `ke_config` VALUES ('50', 'APP_SECRET', '1', '淘宝app_secret', '6', '', '阿里妈妈申请的app_secret', '1437814213', '1437900024', '1', '9a7edf3eb052c03441025e6c4cd36d10', '0');
INSERT INTO `ke_config` VALUES ('51', 'PID', '1', '淘宝pid', '6', '', '推广单元ID，用于区分不同的推广渠道，形如：mm_32640998_6774965_22990111', '1439796967', '1439796987', '1', '', '0');

DROP TABLE IF EXISTS `ke_document`;
CREATE TABLE `ke_document` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文档ID',
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `name` char(40) NOT NULL DEFAULT '' COMMENT '标识',
  `title` char(80) NOT NULL DEFAULT '' COMMENT '标题',
  `category_id` int(10) unsigned NOT NULL COMMENT '所属分类',
  `group_id` smallint(3) unsigned NOT NULL COMMENT '所属分组',
  `description` char(140) NOT NULL DEFAULT '' COMMENT '描述',
  `root` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '根节点',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '所属ID',
  `model_id` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '内容模型ID',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '2' COMMENT '内容类型',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '推荐位',
  `link_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '外链',
  `cover_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '封面',
  `display` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '可见性',
  `deadline` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '截至时间',
  `attach` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '附件数量',
  `view` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '浏览量',
  `comment` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '评论数',
  `extend` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '扩展统计字段',
  `level` int(10) NOT NULL DEFAULT '0' COMMENT '优先级',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '数据状态',
  `tag` int(10) unsigned NOT NULL COMMENT '标签',
  PRIMARY KEY (`id`),
  KEY `idx_category_status` (`category_id`,`status`),
  KEY `idx_status_type_pid` (`status`,`uid`,`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='文档模型基础表';

INSERT INTO `ke_document` VALUES ('2', '1', '', '这是测试文章', '2', '0', '来到北海道，你绝对不能不知道“六花亭”。这盒跳棋盘酒芯糖，每粒糖都是一层薄薄的糖衣，含在口中轻轻一抿，流出来一包酒。清酒、芝华士、梅酒、各种口味都有，也是醉了。', '0', '0', '2', '2', '4', '0', '23', '1', '0', '0', '129', '0', '0', '0', '1439530680', '1439715393', '1', '0');
INSERT INTO `ke_document` VALUES ('3', '1', '', '这是测试文章2', '2', '0', ' 我一直在寻找一款适合我们团队使用的协同工具，但因为各种原因最后都放弃了，直到用上Worktile，才感觉找到了我的真爱，我愿意把它推荐给我们身边每一位有团队协同需求的朋友', '0', '0', '2', '2', '4', '0', '24', '1', '0', '0', '0', '0', '0', '0', '1439536922', '1439536922', '1', '0');
INSERT INTO `ke_document` VALUES ('5', '1', '', '这是测试文章3', '2', '0', ' 我一直在寻找一款适合我们团队使用的协同工具，但因为各种原因最后都放弃了，直到用上Worktile，才感觉找到了我的真爱，我愿意把它推荐给我们身边每一位有团队协同需求的朋友', '0', '0', '2', '2', '4', '0', '24', '1', '0', '0', '4', '0', '0', '0', '1439536980', '1439689922', '1', '1');
INSERT INTO `ke_document` VALUES ('6', '1', '', '这是测试文章4', '2', '0', ' 我一直在寻找一款适合我们团队使用的协同工具，但因为各种原因最后都放弃了，直到用上Worktile，才感觉找到了我的真爱，我愿意把它推荐给我们身边每一位有团队协同需求的朋友', '0', '0', '2', '2', '4', '0', '24', '1', '0', '0', '6', '0', '0', '0', '1439537040', '1439689910', '1', '1');
INSERT INTO `ke_document` VALUES ('7', '1', '', '这是测试文章5', '2', '0', ' 我一直在寻找一款适合我们团队使用的协同工具，但因为各种原因最后都放弃了，直到用上Worktile，才感觉找到了我的真爱，我愿意把它推荐给我们身边每一位有团队协同需求的朋友', '0', '0', '2', '2', '4', '0', '24', '1', '0', '0', '4', '0', '0', '0', '1439537100', '1439689900', '1', '1');
INSERT INTO `ke_document` VALUES ('8', '1', '', '这是测试文章6', '2', '0', ' 我一直在寻找一款适合我们团队使用的协同工具，但因为各种原因最后都放弃了，直到用上Worktile，才感觉找到了我的真爱，我愿意把它推荐给我们身边每一位有团队协同需求的朋友', '0', '0', '2', '2', '0', '0', '24', '1', '0', '0', '4', '0', '0', '0', '1439537220', '1439689890', '1', '1');
INSERT INTO `ke_document` VALUES ('9', '1', '', '恋爱没时差，缠绕在腕间的小确幸', '2', '0', '当下最热的时尚腕表非 Daniel Wellington 莫属了，简约设计，轻奢潮流，无论是时尚圈还是体育界，乃至演艺圈，各路明星统统对它偏爱有加。', '0', '0', '2', '2', '7', '0', '25', '1', '0', '0', '22', '0', '0', '0', '1439539500', '1440569404', '1', '1');
INSERT INTO `ke_document` VALUES ('10', '1', '', '甜美发饰，巧妙拉高你的颜值', '2', '0', '', '0', '0', '2', '2', '0', '0', '29', '1', '0', '0', '0', '0', '0', '0', '1440666180', '1440666180', '1', '0');
INSERT INTO `ke_document` VALUES ('11', '1', '', '最贴心的教师节礼物，款款戳中老师的心', '2', '0', '隆重的教师节日快到了！同学们纷纷在准备老师的礼物，那么该送老师什么合适的礼物呢？关键是超实用为上，要知道教师节这天，在众多别人的礼物中送出 有意思，唯有贴心是王道。准备好礼物了吗？即使还没有，也没关系哦。小礼君放大招，搜罗十方好礼，助攻送！老！师！还有福利送送送~', '0', '0', '2', '2', '0', '0', '39', '1', '0', '0', '2', '0', '0', '0', '1440666720', '1441182904', '1', '0');
INSERT INTO `ke_document` VALUES ('12', '1', '', '  温暖礼物，拉近你与闺蜜之间的小距离', '2', '0', '如何俘获女生的芳心？就从一份好看的礼物开始吧。好看的东西，总会是比较讨人喜欢，也会让女生们在逛街的时候忍不住多看两眼，女生受到这样的礼物，一定会尖叫的，礼物不需要一定要到节日才送的，其实平日里，女生准备一些小惊喜，她们也是很开心的~', '0', '0', '2', '2', '0', '0', '38', '1', '0', '0', '2', '0', '0', '0', '1440666720', '1441182761', '1', '0');
INSERT INTO `ke_document` VALUES ('13', '1', '', '睡觉是个大事，床单也能清新又卖萌', '2', '0', '对于住校党、租房党来说，最开心的事就是可以自己决定买什么样的东西了。比如那些美翻的床单，床单控们深有体会。每次去逛宜家这样的地方，抱着被子简直就拉不走，见了喜欢的图案（无论什么样式）就想买回家。', '0', '0', '2', '2', '0', '0', '37', '1', '0', '0', '3', '0', '0', '0', '1440666780', '1441182655', '1', '3');
INSERT INTO `ke_document` VALUES ('14', '1', '', '玻璃美物精选，我的世界晶莹剔透', '2', '0', '相信有很多的小伙伴都是玻璃控，因为它的经营因为它的剔透，它给人最简答的感觉，但是又给人最剔透的质感，总能轻易的让人喜欢到哭。当然还喜欢它的简单和纯粹，不用猜来猜去的，简单又幸福呢~', '0', '0', '2', '2', '7', '0', '36', '1', '0', '0', '2', '0', '0', '0', '1440666780', '1441182489', '1', '0');
INSERT INTO `ke_document` VALUES ('15', '1', '', '发饰的搭配', '2', '0', '发饰的搭配很有学问，除了要根据穿衣风格决定，有时候连妆容也会影响发饰的搭配。所以妹子们的首饰盒里可不能只有一两款发饰哦！并且发饰的品类现在越来越多，设计也更是越来越巧妙，姑娘们要紧跟潮流，挑选出好的发饰，让自己的气质越来越出众哦~', '0', '0', '2', '2', '7', '0', '29', '1', '0', '0', '6', '0', '0', '0', '1440666780', '1441182266', '1', '0');
DROP TABLE IF EXISTS `ke_document_article`;
CREATE TABLE `ke_document_article` (
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文档ID',
  `parse` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '内容解析类型',
  `content` text NOT NULL COMMENT '文章内容',
  `template` varchar(100) NOT NULL DEFAULT '' COMMENT '详情页显示模板',
  `bookmark` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '收藏数',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文档模型文章表';

INSERT INTO `ke_document_article` VALUES ('2', '0', '<p>\r\n	<img src=\"/ke2/Uploads/Editor/2015-08-14/55cd7e551202f.jpg\" alt=\"\" /> \r\n</p>\r\n<p>\r\n	来到北海道，你绝对不能不知道“六花亭”。这盒跳棋盘酒芯糖，每粒糖都是一层薄薄的糖衣，含在口中轻轻一抿，流出来一包酒。清酒、芝华士、梅酒、各种口味都有，也是醉了。\r\n</p>\r\n<p>\r\n	<img src=\"/ke2/Uploads/Editor/2015-08-14/55cd7e7a1a95e.jpg\" alt=\"\" /> \r\n</p>\r\n<p class=\"goods-extend\">\r\n	467\r\n</p>', '', '0');
INSERT INTO `ke_document_article` VALUES ('3', '0', '', '', '0');
INSERT INTO `ke_document_article` VALUES ('5', '0', '的<br />', '', '0');
INSERT INTO `ke_document_article` VALUES ('6', '0', '的', '', '0');
INSERT INTO `ke_document_article` VALUES ('7', '0', '&nbsp;的', '', '0');
INSERT INTO `ke_document_article` VALUES ('8', '0', '的', '', '0');
INSERT INTO `ke_document_article` VALUES ('9', '0', '当下最热的时尚腕表非&nbsp;Daniel Wellington 莫属了，简约设计，轻奢潮流，无论是时尚圈还是体育界，乃至演艺圈，各路明星统统对它偏爱有加。', '', '0');
INSERT INTO `ke_document_article` VALUES ('10', '0', '<div class=\"fw wrapper-cover\">\r\n	<div class=\"cover\">\r\n		<img src=\"http://img03.liwushuo.com/image/150826/cl8659buo.jpg-w720\" alt=\"\" class=\"hoverZoomLink\" /> \r\n	</div>\r\n</div>\r\n<div class=\"td\">\r\n	<h1 class=\"title\">\r\n		甜美发饰，巧妙拉高你的颜值\r\n	</h1>\r\n	<div class=\"td-post-info\">\r\n		<ul class=\"post-info il-block\">\r\n			<li class=\"info-item\">\r\n				<span>28</span>\r\n			</li>\r\n			<li class=\"info-item\">\r\n				<span>4</span>\r\n			</li>\r\n			<li class=\"info-item\">\r\n				<span>0</span>\r\n			</li>\r\n		</ul>\r\n		<div class=\"date il-block\">\r\n			8月27日 星期四\r\n		</div>\r\n	</div>\r\n</div>\r\n<hr class=\"fancy-line\" />\r\n<p>\r\n	发饰的搭配很有学问，除了要根据穿衣风格决定，有时候连妆容也会影响发饰的搭配。所以妹子们的首饰盒里可不能只有一两款发饰哦！并且发饰的品类现在越来越多，设计也更是越来越巧妙，姑娘们要紧跟潮流，挑选出好的发饰，让自己的气质越来越出众哦~\r\n</p>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">1</span><span class=\"ititle\">镂空花朵发夹</span>\r\n</h3>\r\n<p>\r\n	永远不会枯萎的花朵，它会一直在你耳边轻轻呢喃，告诉你你有多美，多可爱~<img src=\"http://img01.liwushuo.com/image/150826/uwlcpgb5a_w.jpg-w720\" alt=\"\" />\r\n</p>\r\n<div class=\"item-info\">\r\n	<p class=\"item-info-price\">\r\n		<span style=\"font-family:arial;\">￥5.80</span>\r\n	</p>\r\n	<p class=\"item-like-info\">\r\n		33人喜欢\r\n	</p>\r\n<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3DjEVKe9BICaocQipKwQzePOeEDrYVVa64XoO8tOebS%2BdRAdhuF14FMe%2BZWAE6EB7Llovu%2FCElQOutgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L55S%2BrRAxHwNTiXCFWb0%2FAMrf4v4nG%2Fn%2FqOYQwrhPE0iw%3D\" target=\"_blank\"><span>查看详情</span></a>\r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">2</span><span class=\"ititle\">金属大圆圈发夹</span>\r\n</h3>\r\n<p>\r\n	Celine走秀款发饰，是不是很别致呐！设计感十足，用法也是多种多样，纯粹的气质款发夹。<img src=\"http://img02.liwushuo.com/image/150826/t3hiloygj_w.jpg-w720\" alt=\"\" />\r\n</p>\r\n<div class=\"item-info\">\r\n	<p class=\"item-info-price\">\r\n		<span style=\"font-family:arial;\">￥9.50</span>\r\n	</p>\r\n	<p class=\"item-like-info\">\r\n		10人喜欢\r\n	</p>\r\n<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3D0ZjRFH3uVuccQipKwQzePOeEDrYVVa64XoO8tOebS%2BdRAdhuF14FMVOTSd6yK9q9lovu%2FCElQOutgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L5UdGajyxJHfF%2FJsOnAiuMysOXXz2p8BRxvRn9s5lIr3k%3D\" target=\"_blank\"><span>查看详情</span></a>\r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">3</span><span class=\"ititle\">四叶草珍珠发圈</span>\r\n</h3>\r\n<p>\r\n	亚克力材质，粉白色搭配温润的珍珠，整个人的气质都变得温暖委婉了起来~<img src=\"http://img01.liwushuo.com/image/150826/40chphc6a_w.png-w720\" alt=\"\" />\r\n</p>\r\n<div class=\"item-info\">\r\n	<p class=\"item-info-price\">\r\n		<span style=\"font-family:arial;\">￥13.00</span>\r\n	</p>\r\n	<p class=\"item-like-info\">\r\n		10人喜欢\r\n	</p>\r\n<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3D1xpHZG4WZzocQipKwQzePOeEDrYVVa64XoO8tOebS%2BdRAdhuF14FMWvi2xJE0w%2Bu1aH1Hk3GeOitgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L52lphCpTHBoUPKrufD%2BWq8KhM249eclsaxgxdTc00KD8%3D\" target=\"_blank\"><span>查看详情</span></a>\r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">4</span><span class=\"ititle\">长飘带发箍</span>\r\n</h3>\r\n<p>\r\n	发饰可以改变一个人的风格，这句话一点没错。各种色彩的发带，戴在头上轻快活泼的形象立马就生动起来~<img src=\"http://img02.liwushuo.com/image/150826/lfgj5o9bd_w.jpg-w720\" alt=\"\" />\r\n</p>\r\n<div class=\"item-info\">\r\n	<p class=\"item-info-price\">\r\n		<span style=\"font-family:arial;\">￥13.80</span>\r\n	</p>\r\n	<p class=\"item-like-info\">\r\n		9人喜欢\r\n	</p>\r\n<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3DaqMJ6a%2FtikwcQipKwQzePOeEDrYVVa64XoO8tOebS%2BdRAdhuF14FMbsiIcCZenwC1aH1Hk3GeOitgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L5Pmw1YTSuM5R2kKYXYDRZSZg5aPDY4ohdxgxdTc00KD8%3D\" target=\"_blank\"><span>查看详情</span></a>\r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">5</span><span class=\"ititle\">椭圆方块发圈</span>\r\n</h3>\r\n<p>\r\n	温婉又别致的发圈，每一款的颜色搭配都很美，即便戴在手腕上也是美极啦！<img src=\"http://img01.liwushuo.com/image/150826/4b7dymrqj_w.jpg-w720\" alt=\"\" />\r\n</p>\r\n<div class=\"item-info\">\r\n	<p class=\"item-info-price\">\r\n		<span style=\"font-family:arial;\">￥18.00</span>\r\n	</p>\r\n	<p class=\"item-like-info\">\r\n		0人喜欢\r\n	</p>\r\n<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3DIYj3vYqCN%2BIcQipKwQzePOeEDrYVVa64XoO8tOebS%2BdRAdhuF14FMVDovJlQ3dU6lovu%2FCElQOutgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L5HI7Kl03wqA%2FTkKDrtJg9i55FHelKliwRcSpj5qSCmbA%3D\" target=\"_blank\"><span>查看详情</span></a>\r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">6</span><span class=\"ititle\">宝石发箍</span>\r\n</h3>\r\n<p>\r\n	简单缠绕的发箍中盛开着一株美丽的花朵，花蕊由温柔的宝石代替，精致美丽，亦如你。<img src=\"http://img03.liwushuo.com/image/150826/asp8idk37_w.jpg-w720\" alt=\"\" />\r\n</p>\r\n<div class=\"item-info\">\r\n	<p class=\"item-info-price\">\r\n		<span style=\"font-family:arial;\">￥19.11</span>\r\n	</p>\r\n	<p class=\"item-like-info\">\r\n		0人喜欢\r\n	</p>\r\n<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3DIszz%2FWhiuHIcQipKwQzePOeEDrYVVa64XoO8tOebS%2BdRAdhuF14FMccPk3yLTQR61aH1Hk3GeOitgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L5MiHsyFLMvXDxL7ipQbrfH%2Boi867u9zcqxgxdTc00KD8%3D\" target=\"_blank\"><span>查看详情</span></a>\r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">7</span><span class=\"ititle\">双层珍珠发箍</span>\r\n</h3>\r\n<p>\r\n	简约又大方，一颗颗圆润的珍珠镶嵌在发丝之中，简直就是秒变女神~<img src=\"http://img03.liwushuo.com/image/150826/hemehtgxj_w.jpg-w720\" alt=\"\" />\r\n</p>\r\n<div class=\"item-info\">\r\n	<p class=\"item-info-price\">\r\n		<span style=\"font-family:arial;\">￥18.81</span>\r\n	</p>\r\n	<p class=\"item-like-info\">\r\n		19人喜欢\r\n	</p>\r\n<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3D4HnS7WgF3fYcQipKwQzePOeEDrYVVa64XoO8tOebS%2BdRAdhuF14FMWHyl%2F32oDGDJ1gyddu7kN%2BtgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L5xPnl%2BDqaSU0crFp8TH3E1%2Fh9Oztapzf5DJbuZDCrHt4%3D\" target=\"_blank\"><span>查看详情</span></a>\r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">8</span><span class=\"ititle\">purefriend·淑女发绳</span>\r\n</h3>\r\n<p>\r\n	身边很少有这样精致的头绳，这么美丽，随意挽的马尾也会因为它变得别致又浪漫~<img src=\"http://img03.liwushuo.com/image/150826/6hjuaxjn4.jpg-w720\" alt=\"\" />\r\n</p>\r\n<div class=\"item-info\">\r\n	<p class=\"item-info-price\">\r\n		<span style=\"font-family:arial;\">￥28.80</span>\r\n	</p>\r\n	<p class=\"item-like-info\">\r\n		5人喜欢\r\n	</p>\r\n<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3DhaggSIbBTUIcQipKwQzePOeEDrYVVa64XoO8tOebS%2BdRAdhuF14FMWZYHt03aUGtMMgx22UI05atgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L5i1%2FAntrH1mcptHhgCStUoFXJGmeKwso9cSpj5qSCmbA%3D\" target=\"_blank\"><span>查看详情</span></a>\r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">9</span><span class=\"ititle\">differ·弹簧夹</span>\r\n</h3>\r\n<p>\r\n	采用来自韩国的高级面料，质地轻柔，触感温和，对发质和皮肤无刺激，低调又特别~<img src=\"http://img03.liwushuo.com/image/150826/u4xdjxwos_w.jpg-w720\" alt=\"\" />\r\n</p>\r\n<div class=\"item-info\">\r\n	<p class=\"item-info-price\">\r\n		<span style=\"font-family:arial;\">￥29.00</span>\r\n	</p>\r\n	<p class=\"item-like-info\">\r\n		0人喜欢\r\n	</p>\r\n<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3DbQ9wNeIjuGEcQipKwQzePOeEDrYVVa64XoO8tOebS%2BdRAdhuF14FMWHyl%2F32oDGDMMgx22UI05atgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L55Zhq6kwiN%2BLtUyKsoUpKNQf0g%2Fm99p63xgxdTc00KD8%3D\" target=\"_blank\"><span>查看详情</span></a>\r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">10</span><span class=\"ititle\">LuCe·锆石闪耀边夹</span>\r\n</h3>\r\n<p>\r\n	十分美丽闪耀的发夹，每一颗锆石都晶莹剔透，整齐地排列在发丝上，阳光下的你会闪闪发光~<img src=\"http://img01.liwushuo.com/image/150826/z1u4iw6a4_w.jpg-w720\" alt=\"\" />\r\n</p>', '', '0');
INSERT INTO `ke_document_article` VALUES ('11', '0', '<p>\r\n	隆重的教师节日快到了！同学们纷纷在准备老师的礼物，那么该送老师什么合适的礼物呢？关键是超实用为上，要知道教师节这天，在众多别人的礼物中送出\r\n有意思，唯有贴心是王道。准备好礼物了吗？即使还没有，也没关系哦。小礼君放大招，搜罗十方好礼，助攻送！老！师！还有福利送送送~\r\n</p>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">1</span><span class=\"ititle\">物生物· 便携带盖水果玻璃杯</span> \r\n</h3>\r\n<p>\r\n	几乎老师们都喜欢拿个大水杯喜欢泡各种茶，送老师一个水杯也是很贴心的哦。这个透明的水杯，还可以拿来泡各种水果，很有意思呢，高硼硅玻璃，纯手工吹制，水果泡着吃~<img src=\"http://img01.liwushuo.com/image/150828/cnx5puyl4_w.jpg-w720\" alt=\"\" /> \r\n</p>\r\n<div class=\"item-info\">\r\n	<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3Dwq5MXbE1qU0cQipKwQzePOeEDrYVVa64yK8Cckff7TVRAdhuF14FMWll5VKcnXi379%2FTFaMDK6StgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L5uSaWWvUCrgErbe%2FsFuW9kRd6PyyiUKpvDJbuZDCrHt4%3D\" target=\"_blank\"><span><br />\r\n</span></a> \r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">2</span><span class=\"ititle\">云朵工厂·可爱云朵抱枕靠垫</span> \r\n</h3>\r\n<p>\r\n	送给老师的一片云朵好心情，蓝色看起来很赏心悦目，放在严肃的办公室里，感觉老师也变萌了~<img src=\"http://img03.liwushuo.com/image/150828/1z6w6iker_w.png-w720\" alt=\"\" /><img src=\"http://img02.liwushuo.com/image/150828/6xxza3yhw.jpg-w720\" alt=\"\" /> \r\n</p>\r\n<div class=\"item-info\">\r\n	<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3DV2oNcUIKp74cQipKwQzePOeEDrYVVa64yK8Cckff7TVRAdhuF14FMVF%2FVsFMRbRO5x%2BIUlGKNpWtgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L5DtfSvsP0QR9mjOxevwD5Gt2kcbnvFi1HvRn9s5lIr3k%3D\" target=\"_blank\"><span><br />\r\n</span></a> \r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">3</span><span class=\"ititle\">KACO·智存系列U盘笔8G（2015红点设计奖产品 ）</span> \r\n</h3>\r\n<p>\r\n	笔盖上面是个创意的U盘，自带8G存储。 一笔两用，如此创意实在很赞。来自红点奖评审团的官方点评：“这支宝珠笔的外观直观地传达了它的美感，但同时又以不易察觉的形式将创意U盘的形式巧妙融入笔帽之中”。<img src=\"http://img01.liwushuo.com/image/150828/o41ng7nbn.jpg-w720\" alt=\"\" /> \r\n</p>\r\n<br />', '', '0');
INSERT INTO `ke_document_article` VALUES ('12', '0', '<p>\r\n	如何俘获女生的芳心？就从一份好看的礼物开始吧。好看的东西，总会是比较讨人喜欢，也会让女生们在逛街的时候忍不住多看两眼，女生受到这样的礼物，一定会尖叫的，礼物不需要一定要到节日才送的，其实平日里，女生准备一些小惊喜，她们也是很开心的~\r\n</p>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">1</span><span class=\"ititle\"> 陶瓷可爱铃铛情侣款手链</span>\r\n</h3>\r\n<p>\r\n	超可爱的陶瓷情侣手链，可以调手链大小，和你的另一伴一起去卖萌吧~<img src=\"http://img01.liwushuo.com/image/150901/qsz6nwq7l.jpg-w720\" alt=\"\" />\r\n</p>\r\n<div class=\"item-info\">\r\n	<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3DCXq4W5zecF0cQipKwQzePOeEDrYVVa64XoO8tOebS%2BdRAdhuF14FMWkX944bwodkxq3IhSJN6GStgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L5ThKc%2Bghdl88hP%2Fd0m%2BxLtGJGEidXJ93ExgxdTc00KD8%3D\" target=\"_blank\"><span><br />\r\n</span></a>\r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">2</span><span class=\"ititle\">ZAMIS·可爱动物马克杯</span>\r\n</h3>\r\n<p>\r\n	超可爱的马克杯，有4个可爱的图案，总有一款适合你的，送人也不错呢~<img src=\"http://img01.liwushuo.com/image/150901/13jdfl4fo.jpg-w720\" alt=\"\" />\r\n</p>\r\n<div class=\"item-info\">\r\n	<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3D06fkSwsGOH8cQipKwQzePOeEDrYVVa64XoO8tOebS%2BdRAdhuF14FMYGYJpau0stBRitN3%2FurF3ytgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L5UbLhRXmywUaUCv0BCSviwuE3xYM9gxfCxgxdTc00KD8%3D\" target=\"_blank\"><span><br />\r\n</span></a>\r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">3</span><span class=\"ititle\">清新绣花田园抱枕套 </span>\r\n</h3>\r\n<p>\r\n	清新韩式风格，毛巾绣工艺，带给您更别致的生活体验，个性时尚，立体感十足，亲肤毛巾绣，手感舒适~<img src=\"http://img02.liwushuo.com/image/150901/8bnuvbxtx.jpg-w720\" alt=\"\" />\r\n</p>\r\n<div class=\"item-info\">\r\n	<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3DQIiZgiEpBXYcQipKwQzePOeEDrYVVa64yK8Cckff7TVRAdhuF14FMQ%2FOI9GIZ%2FQvxq3IhSJN6GStgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L5P5RXBmuj0QecLaSZX2wN7kR8O5zqQCC1xgxdTc00KD8%3D\" target=\"_blank\"><span><br />\r\n</span></a>\r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">4</span><span class=\"ititle\">可爱猫头鹰存钱罐</span>\r\n</h3>\r\n<p>\r\n	猫头鹰可爱存钱罐，新手造物，手工印迹，富有层次感，更有档次，耐看~<img src=\"http://img03.liwushuo.com/image/150901/d3zr58w04.jpg-w720\" alt=\"\" />\r\n</p>\r\n<br />', '', '0');
INSERT INTO `ke_document_article` VALUES ('13', '0', '<div class=\"fw wrapper-cover\">\r\n	<div class=\"cover\">\r\n		对于住校党、租房党来说，最开心的事就是可以自己决定买什么样的东西了。比如那些美翻的\r\n床单，床单控们深有体会。每次去逛宜家这样的地方，抱着被子简直就拉不走，见了喜欢的图案（无论什么样式）就想买回家。又到了换季的时候了，为自己添置一\r\n套赏心悦目的床单，睡觉是一件大事儿，我们就是不愿意将就~\r\n	</div>\r\n</div>\r\n<h2 class=\"section-title\">\r\n	文艺清新派\r\n</h2>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">1</span><span class=\"ititle\">简约条纹天竺棉针织三件套/四件套</span> \r\n</h3>\r\n<p>\r\n	十分淡雅的水蓝色，还带着点浅浅的米白。针织棉有弹性，使得面料更加柔软亲肤，原料使用新疆天然无污染的天竺棉，不起球不褪色不易变形。<img src=\"http://img03.liwushuo.com/image/150828/msjbza2se_w.jpg-w720\" alt=\"\" /> \r\n</p>\r\n<div class=\"item-info\">\r\n	<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3D%2Fn0u2rwYmPkcQipKwQzePOeEDrYVVa64XoO8tOebS%2BdRAdhuF14FMS5eCKZmbKcF8sviUM61dt2tgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L5Jz8%2BM7EMntHuANxfZWQEPhFDMEqkhDlEcSpj5qSCmbA%3D\" target=\"_blank\"><span><br />\r\n</span></a> \r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">2</span><span class=\"ititle\">简约文艺小花儿四件套</span> \r\n</h3>\r\n<p>\r\n	细细的小花儿点缀着被套、枕套，是名为温柔的情愫以棉布的质地展现。顷刻间，所有的迷失都没有了理由……回家，睡觉！<img src=\"http://img03.liwushuo.com/image/150828/woymjku09_w.jpg-w720\" alt=\"\" /> \r\n</p>\r\n<div class=\"item-info\">\r\n	<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3DE6gxbMp2qf4cQipKwQzePOeEDrYVVa64XoO8tOebS%2BdRAdhuF14FMVtzlmqJl9E1xq3IhSJN6GStgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L5Db%2Fymh0hkv7XP9h%2Ba3wo40aXz3Q%2F86hdDJbuZDCrHt4%3D\" target=\"_blank\"><span><br />\r\n</span></a> \r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">3</span><span class=\"ititle\">全棉纯色磨毛四件套</span> \r\n</h3>\r\n<p>\r\n	温暖的色调，睡起来很舒心。特别在用时很贴身，磨毛床单有一种暖暖、柔柔的舒服感觉，颜色也历久如新。<img src=\"http://img02.liwushuo.com/image/150828/v0tizxdkd_w.png-w720\" alt=\"\" /> \r\n</p>\r\n<div class=\"item-info\">\r\n	<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3DQcaVY8j1NngcQipKwQzePOeEDrYVVa64XoO8tOebS%2BdRAdhuF14FMdLBjhdm1qgGMMgx22UI05atgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L5Db%2Fymh0hkv4Yv4On9pSXGbmIZeBkGCjcxgxdTc00KD8%3D\" target=\"_blank\"><span><br />\r\n</span></a> \r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">4</span><span class=\"ititle\">太平鸟·巢淡雅花色四件套</span> \r\n</h3>\r\n<p>\r\n	AB双面花色，淡雅的花纹，不争不闹，不喧不嚷。给你一个安静温和的睡眠时光~<img src=\"http://img02.liwushuo.com/image/150828/hw6uxibzx_w.png-w720\" alt=\"\" /> \r\n</p>\r\n<div class=\"item-info\">\r\n	<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3DkkVyyGl2whocQipKwQzePOeEDrYVVa64yK8Cckff7TVRAdhuF14FMWkK7as%2FGOhXMMgx22UI05atgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L5T84fjGqxj%2FjnJLob3YgPUa5rlukUPHhcL33lFJev%2B6Q%3D\" target=\"_blank\"><span><br />\r\n</span></a> \r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">5</span><span class=\"ititle\">素色双拼三件套/四件套</span> \r\n</h3>\r\n特别简单的素色双拼，有二十多种颜色搭配，颜色控的大爱。男生女生的都可以找到自己喜欢的颜色~<img src=\"http://img02.liwushuo.com/image/150828/esj7sj6bx.jpg-w720\" alt=\"\" />', '', '0');
INSERT INTO `ke_document_article` VALUES ('14', '0', '<p>\r\n	相信有很多的小伙伴都是玻璃控，因为它的经营因为它的剔透，它给人最简答的感觉，但是又给人最剔透的质感，总能轻易的让人喜欢到哭。当然还喜欢它的简单和纯粹，不用猜来猜去的，简单又幸福呢~\r\n</p>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">1</span><span class=\"ititle\">斯纳迪格透明玻璃水珠花瓶</span> \r\n</h3>\r\n<p>\r\n	玻璃透明的花瓶，一眼望去，看到植物的根茎，像是水珠挂在瓶身上，简单剔透。<img src=\"http://img03.liwushuo.com/image/150127/c86wpityv_w.jpg-w720\" alt=\"\" /> \r\n</p>\r\n<div class=\"item-info\">\r\n	<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3D5wedg4Bi220cQipKwQzePOeEDrYVVa64XoO8tOebS%2BdRAdhuF14FMcSxqhu0556tt4hWD5k2kjOtgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L5ceY8Axf8%2FtqUahjimwSsQPUXsvJuOE2kcSpj5qSCmbA%3D\" target=\"_blank\"><span><br />\r\n</span></a> \r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">2</span><span class=\"ititle\">透明玻璃冰淇淋碗</span> \r\n</h3>\r\n<p>\r\n	透明漂亮的小碗，可以将自制的酸菜、布丁放入里面，晶莹剔透的美死了~用了漂亮的器皿，吃起来也会美味很多。<img src=\"http://img01.liwushuo.com/image/150724/nspkav17h.jpg-w720\" alt=\"\" /> \r\n</p>\r\n<div class=\"item-info\">\r\n	<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3DnG5MHL7EmhUcQipKwQzePOeEDrYVVa64XoO8tOebS%2BdRAdhuF14FMX1C3eZIYzeGxq3IhSJN6GStgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L5uKhdzJBz69Uvc6TkVRhOTOkeu3tvJDZfxgxdTc00KD8%3D\" target=\"_blank\"><span><br />\r\n</span></a> \r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">3</span><span class=\"ititle\">白菊·日式风铃</span> \r\n</h3>\r\n<p>\r\n	白菊的花语是高尚，凡是受到这种花祝福的人，都拥有超俗的高贵情操。夏天，就让这份清新脱俗的风铃给你带去凉爽的好心情吧~<img src=\"http://img01.liwushuo.com/image/150527/qurizri0z_w.png-w720\" alt=\"\" /> \r\n</p>\r\n<div class=\"item-info\">\r\n	<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3DgtYs2dmHeikcQipKwQzePOeEDrYVVa64XoO8tOebS%2BdRAdhuF14FMU28aZPxolBgRitN3%2FurF3ytgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L5n4dhmrLQLo%2FXt6A7MvN5ceXBNi9tBZnVxgxdTc00KD8%3D\" target=\"_blank\"><span><br />\r\n</span></a> \r\n</div>\r\n<p class=\"item-title\">\r\n	<span class=\"ititle-serial\">4</span><span class=\"ititle\">北欧童话玻璃罩摆件·森之精灵</span> \r\n</p>\r\n<p>\r\n	这里有皑皑的白雪，又有暖暖的壁炉。这里有惊艳的极光，又有旧旧的城墙。\r\n</p>\r\n<p>\r\n	<img src=\"http://img01.liwushuo.com/image/150813/otghb9di5.jpg-w720\" alt=\"\" /> \r\n</p>\r\n<div class=\"item-info\">\r\n	<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3DbUXDBAycPUMcQipKwQzePOeEDrYVVa64XoO8tOebS%2BdRAdhuF14FMTV9bb6CX5eIxq3IhSJN6GStgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L5WuNr8lEMdQwRaJ6u0bmTIOdaQawJND6qDJbuZDCrHt4%3D\" target=\"_blank\"><span><br />\r\n</span></a> \r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">5</span><span class=\"ititle\">简约双层玻璃烛台*高</span> \r\n</h3>\r\n<p>\r\n	双层透明的烛台，晶莹剔透，贴心提示：在玻璃筒的内部可以随意装饰，放花朵，放小玩偶，放有你们共同回忆的摆件~<img src=\"http://img03.liwushuo.com/image/150130/p8jqyvmhh_w.jpg-w720\" alt=\"\" /> \r\n</p>\r\n<div class=\"item-info\">\r\n	<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3DoTiksuxHWmYcQipKwQzePOeEDrYVVa64XoO8tOebS%2BdRAdhuF14FMVtBeS5c%2Bv92xq3IhSJN6GStgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L5XaEw9O6%2FxB3wwrxwaUd1lcuHw2gn1mKvcSpj5qSCmbA%3D\" target=\"_blank\"><span><br />\r\n</span></a> \r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">6</span><span class=\"ititle\">樱花谣·日式风铃</span> \r\n</h3>\r\n<p>\r\n	等风来，看那粉嫩的樱花瓣随着轻风慢慢的抖动，发出叮当叮当的响声，很是好听，挂在房间里，享受那一刻的浪漫～<img src=\"http://img03.liwushuo.com/image/150227/hyfgbnb14_w.jpg-w720\" alt=\"\" /> \r\n</p>\r\n<div class=\"item-info\">\r\n	<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3DVjOllBnaOrgcQipKwQzePOeEDrYVVa64XoO8tOebS%2BdRAdhuF14FMcVGSlSzKMTtMMgx22UI05atgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L5ZUDCJ85QZEqKzZ6pI0BY8k9IDK6CmwvqxgxdTc00KD8%3D\" target=\"_blank\"><span><br />\r\n</span></a> \r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">7</span><span class=\"ititle\">皇冠玻璃花瓶</span> \r\n</h3>\r\n<p>\r\n	超有创意的皇冠小花瓶，用来做景观花瓶非常赞，不一样的搭配给你带来不一样的视觉享受~<img src=\"http://img02.liwushuo.com/image/150513/0n9up5ja8_w.jpg-w720\" alt=\"\" /> \r\n</p>\r\n<div class=\"item-info\">\r\n	<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3DeyYQWcHv4nscQipKwQzePOeEDrYVVa64yK8Cckff7TVRAdhuF14FMfOBTLwZRQLe1aH1Hk3GeOitgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L5GfMTdFjUTrBhL8LtctybaaK4KfydEmKnxgxdTc00KD8%3D\" target=\"_blank\"><span><br />\r\n</span></a> \r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">8</span><span class=\"ititle\">锤纹玻璃水杯 </span> \r\n</h3>\r\n<p>\r\n	锤目纹源于手工捶打器物表面而出，但不形成具体形象，有很多拟态形式，如荒芜龟裂的土体，微风拂过的波澜，一锤一打间，呈现万千变化。<img src=\"http://img01.liwushuo.com/image/150805/qqeyp7af3_w.jpg-w720\" alt=\"\" /> \r\n</p>\r\n<div class=\"item-info\">\r\n	<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3DggQoG5bA3oYcQipKwQzePOeEDrYVVa64XoO8tOebS%2BdRAdhuF14FMYZY1uHBHtjHt4hWD5k2kjOtgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L5sZ1cDqRlhPpSw188%2FftgVw9RCeycDvkiMzXmCitDxQQ%3D\" target=\"_blank\"><span><br />\r\n</span></a> \r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">9</span><span class=\"ititle\">手工玻璃球樱花项链</span> \r\n</h3>\r\n<p>\r\n	玻璃瓶子里面装的是永生真花，每一朵独一无二，永久保存不会凋谢的哦。<img src=\"http://img02.liwushuo.com/image/150515/hv5tyy3ni_w.jpg-w720\" alt=\"\" /> \r\n</p>\r\n<div class=\"item-info\">\r\n	<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3DXBkOKlkT%2B0scQipKwQzePOeEDrYVVa64XoO8tOebS%2BdRAdhuF14FMVtBeS5c%2Bv92lovu%2FCElQOutgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L52dHlOW7KMJ0ccNcNbajdS38%2FLrjLqoqucSpj5qSCmbA%3D\" target=\"_blank\"><span><br />\r\n</span></a> \r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">10</span><span class=\"ititle\">和风系列樱花玻璃杯</span> \r\n</h3>\r\n<p>\r\n	非常精致小巧的一款，小动物在樱花雨中奔跑。看着这样的图案，喝水的心情都会变好吧~<img src=\"http://img01.liwushuo.com/image/150813/83hzcd0tn.jpg-w720\" alt=\"\" /> \r\n</p>\r\n<div class=\"item-info\">\r\n	<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3D8XJLQz4enYwcQipKwQzePOeEDrYVVa64XoO8tOebS%2BdRAdhuF14FMSZbpWBxt83w8sviUM61dt2tgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L56sIkrxPe%2Bua2hw63APmtpxWJybDwQPAJxgxdTc00KD8%3D\" target=\"_blank\"><span><br />\r\n</span></a> \r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">11</span><span class=\"ititle\">U-PICK原品生活·水果图案玻璃杯</span> \r\n</h3>\r\n<p>\r\n	西瓜和柠檬，你更喜欢谁？啊，没有西瓜的夏天是不完整的呢。（柠檬：= =）<img src=\"http://img03.liwushuo.com/image/150813/ovcat9u3t.jpg-w720\" alt=\"\" /> \r\n</p>\r\n<div class=\"item-info\">\r\n	<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3Dwp0c%2F3XTnqQcQipKwQzePOeEDrYVVa64XoO8tOebS%2BdRAdhuF14FMTVXmoRz0%2B2Wlovu%2FCElQOutgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L5nN9kt0wXZKOGH5jeapLqSekKT%2BabTHJ0z2TFFEd9SqY%3D\" target=\"_blank\"><span><br />\r\n</span></a> \r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">12</span><span class=\"ititle\">六角玻璃鱼缸</span> \r\n</h3>\r\n<p>\r\n	既是鱼缸，也是水培花器。悬挂和桌面都可以用。看久了非方即圆的传统鱼缸，换一个形状也换一种心情。<img src=\"http://img03.liwushuo.com/image/150611/q9dhk01l7_w.png-w720\" alt=\"\" /> \r\n</p>\r\n<div class=\"item-info\">\r\n	<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3DZi50eHH6rHUcQipKwQzePOeEDrYVVa64XoO8tOebS%2BdRAdhuF14FMZBiMmOfqcUz79%2FTFaMDK6StgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L5ixIjueDXzPhLtHgI0oM7kBkCly%2BFtvCTcSpj5qSCmbA%3D\" target=\"_blank\"><span><br />\r\n</span></a> \r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">13</span><span class=\"ititle\">球形灯罩烛台</span> \r\n</h3>\r\n<p>\r\n	这款烛台分为透明和磨砂两款，造型圆润可爱，没有亮光的时候比较普通，但点上蜡烛后非常地温馨。<img src=\"http://img01.taobaocdn.com/bao/uploaded/i1/T1yNj.XhhiXXXXXXXX_%21%210-item_pic.jpg_640x640q70.jpg\" alt=\"\" /> \r\n</p>\r\n<div class=\"item-info\">\r\n	<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3DdXWQvSxc7SccQipKwQzePOeEDrYVVa64XoO8tOebS%2BdRAdhuF14FMQPPnh5H0Ohu5x%2BIUlGKNpWtgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L59rxgw5w0Qgsy0OwcDW1R7p8DKpHgtKO4xgxdTc00KD8%3D\" target=\"_blank\"><span><br />\r\n</span></a> \r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">14</span><span class=\"ititle\">北欧微景观悬挂盆栽</span> \r\n</h3>\r\n<p>\r\n	充满生命力的壁挂，悄悄告诉你：玻璃球中的绿植其实是手感胶哦，仿真度超高有木有！<img src=\"http://img01.liwushuo.com/image/150701/0qdo0dsqr_w.jpg-w720\" alt=\"\" /> \r\n</p>\r\n<div class=\"item-info\">\r\n	<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3D2finF3XvCoAcQipKwQzePOeEDrYVVa64XoO8tOebS%2BdRAdhuF14FMQMAEf1pFQnMJ1gyddu7kN%2BtgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L5wQ%2FTRAG%2BF3yNsd7aeKO0so0UqrIuGuvZxgxdTc00KD8%3D\" target=\"_blank\"><span><br />\r\n</span></a> \r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">15</span><span class=\"ititle\">玻璃台灯烛台</span> \r\n</h3>\r\n<p>\r\n	烛台是台灯的姐姐，而月亮则是它们永恒的母亲。<img src=\"http://img01.liwushuo.com/image/150527/bkn1r30a3.jpg-w720\" alt=\"\" /> \r\n</p>\r\n<div class=\"item-info\">\r\n	<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3DZAcrkdHkW24cQipKwQzePOeEDrYVVa64yK8Cckff7TVRAdhuF14FMYyBfYsD0ixa79%2FTFaMDK6StgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L5GfMTdFjUTrAKh0SmUeUVTZJM59RbP%2B93xgxdTc00KD8%3D\" target=\"_blank\"><span><br />\r\n</span></a> \r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">16</span><span class=\"ititle\">时间的花·磁力沙漏</span> \r\n</h3>\r\n<p>\r\n	一看到沙漏就想到《沙漏》，青春期也是被洗脑得够厉害。沙漏的寓意美好，特别适合当礼物。\r\n</p>\r\n<p>\r\n	<img src=\"http://img02.liwushuo.com/image/150813/imw1lh4kd.jpg-w720\" alt=\"\" /> \r\n</p>\r\n<div class=\"item-info\">\r\n	<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3DhqnIvPUWgLccQipKwQzePOeEDrYVVa64yK8Cckff7TVRAdhuF14FMXDJaCLzqIoyJ1gyddu7kN%2BtgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L5XsNkAAhxdjXjyZuN0qCDG%2BPj%2BrNnNaxtz2TFFEd9SqY%3D\" target=\"_blank\"><span><br />\r\n</span></a> \r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">17</span><span class=\"ititle\">彩色玻璃糖果罐</span> \r\n</h3>\r\n<p>\r\n	生活中的少女心，是体现在细节处的，如此绚丽梦幻的糖果罐，美得不像话，根本舍不得用好吗！<img src=\"http://gi1.md.alicdn.com/bao/uploaded/i1/T11xVvFRheXXXXXXXX_%21%210-item_pic.jpg_640x640q70.jpg\" alt=\"\" /> \r\n</p>\r\n<div class=\"item-info\">\r\n	<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3DtvnPV2uHMA8cQipKwQzePOeEDrYVVa64yK8Cckff7TVRAdhuF14FMch5Ktq%2FhJ3dt4hWD5k2kjOtgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L5S7pBZnYdtPymXUO8agu8a1cZqUMcBiu9xgxdTc00KD8%3D\" target=\"_blank\"><span><br />\r\n</span></a> \r\n</div>\r\n<p class=\"item-title\">\r\n	<span class=\"ititle-serial\">18</span><span class=\"ititle\">创意玻璃风暴瓶</span> \r\n</p>\r\n<p>\r\n	&nbsp;星空变化，斗转星移……好了，不费话了。天气变化，小心感冒。\r\n</p>\r\n<p>\r\n	<img src=\"http://img01.liwushuo.com/image/150813/apflj3lmo.jpg-w720\" alt=\"\" /> \r\n</p>\r\n<p>\r\n	<img src=\"http://img03.liwushuo.com/image/150813/orlhwgrtx.jpg-w720\" alt=\"\" /> \r\n</p>\r\n<br />', '', '0');
INSERT INTO `ke_document_article` VALUES ('15', '0', '<p>\r\n	发饰的搭配很有学问，除了要根据穿衣风格决定，有时候连妆容也会影响发饰的搭配。所以妹子们的首饰盒里可不能只有一两款发饰哦！并且发饰的品类现在越来越多，设计也更是越来越巧妙，姑娘们要紧跟潮流，挑选出好的发饰，让自己的气质越来越出众哦~\r\n</p>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">1</span><span class=\"ititle\">镂空花朵发夹</span> \r\n</h3>\r\n<p>\r\n	永远不会枯萎的花朵，它会一直在你耳边轻轻呢喃，告诉你你有多美，多可爱~\r\n</p>\r\n<div class=\"item-info\">\r\n	<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3DjEVKe9BICaocQipKwQzePOeEDrYVVa64XoO8tOebS%2BdRAdhuF14FMe%2BZWAE6EB7Llovu%2FCElQOutgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L55S%2BrRAxHwNTiXCFWb0%2FAMrf4v4nG%2Fn%2FqOYQwrhPE0iw%3D\" target=\"_blank\"><span><br />\r\n</span></a> \r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">2</span><span class=\"ititle\">金属大圆圈发夹</span> \r\n</h3>\r\n<p>\r\n	Celine走秀款发饰，是不是很别致呐！设计感十足，用法也是多种多样，纯粹的气质款发夹。\r\n</p>\r\n<div class=\"item-info\">\r\n	<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3D0ZjRFH3uVuccQipKwQzePOeEDrYVVa64XoO8tOebS%2BdRAdhuF14FMVOTSd6yK9q9lovu%2FCElQOutgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L5UdGajyxJHfF%2FJsOnAiuMysOXXz2p8BRxvRn9s5lIr3k%3D\" target=\"_blank\"><span><br />\r\n</span></a> \r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">3</span><span class=\"ititle\">四叶草珍珠发圈</span> \r\n</h3>\r\n<p>\r\n	亚克力材质，粉白色搭配温润的珍珠，整个人的气质都变得温暖委婉了起来~<img src=\"http://img01.liwushuo.com/image/150826/40chphc6a_w.png-w720\" alt=\"\" /> \r\n</p>\r\n<div class=\"item-info\">\r\n	<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3D1xpHZG4WZzocQipKwQzePOeEDrYVVa64XoO8tOebS%2BdRAdhuF14FMWvi2xJE0w%2Bu1aH1Hk3GeOitgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L52lphCpTHBoUPKrufD%2BWq8KhM249eclsaxgxdTc00KD8%3D\" target=\"_blank\"><span><br />\r\n</span></a> \r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">4</span><span class=\"ititle\">长飘带发箍</span> \r\n</h3>\r\n<p>\r\n	发饰可以改变一个人的风格，这句话一点没错。各种色彩的发带，戴在头上轻快活泼的形象立马就生动起来~<img src=\"http://img02.liwushuo.com/image/150826/lfgj5o9bd_w.jpg-w720\" alt=\"\" /> \r\n</p>\r\n<div class=\"item-info\">\r\n	<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3DaqMJ6a%2FtikwcQipKwQzePOeEDrYVVa64XoO8tOebS%2BdRAdhuF14FMbsiIcCZenwC1aH1Hk3GeOitgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L5Pmw1YTSuM5R2kKYXYDRZSZg5aPDY4ohdxgxdTc00KD8%3D\" target=\"_blank\"><span><br />\r\n</span></a> \r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">5</span><span class=\"ititle\">椭圆方块发圈</span> \r\n</h3>\r\n<p>\r\n	温婉又别致的发圈，每一款的颜色搭配都很美，即便戴在手腕上也是美极啦！<img src=\"http://img01.liwushuo.com/image/150826/4b7dymrqj_w.jpg-w720\" alt=\"\" /> \r\n</p>\r\n<div class=\"item-info\">\r\n	<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3DIYj3vYqCN%2BIcQipKwQzePOeEDrYVVa64XoO8tOebS%2BdRAdhuF14FMVDovJlQ3dU6lovu%2FCElQOutgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L5HI7Kl03wqA%2FTkKDrtJg9i55FHelKliwRcSpj5qSCmbA%3D\" target=\"_blank\"><span><br />\r\n</span></a> \r\n</div>\r\n<h3 class=\"item-title\">\r\n	<span class=\"ititle-serial\">6</span><span class=\"ititle\">宝石发箍</span> \r\n</h3>\r\n<p>\r\n	简单缠绕的发箍中盛开着一株美丽的花朵，花蕊由温柔的宝石代替，精致美丽，亦如你。<img src=\"http://img03.liwushuo.com/image/150826/asp8idk37_w.jpg-w720\" alt=\"\" /> \r\n</p>\r\n<div class=\"item-info\">\r\n	<a class=\"item-info-link\" href=\"http://s.click.taobao.com/t?e=m%3D2%26s%3DIszz%2FWhiuHIcQipKwQzePOeEDrYVVa64XoO8tOebS%2BdRAdhuF14FMccPk3yLTQR61aH1Hk3GeOitgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L5MiHsyFLMvXDxL7ipQbrfH%2Boi867u9zcqxgxdTc00KD8%3D\" target=\"_blank\"><span><br />\r\n</span></a> \r\n</div>\r\n<br />', '', '0');

DROP TABLE IF EXISTS `ke_document_download`;
CREATE TABLE `ke_document_download` (
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文档ID',
  `parse` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '内容解析类型',
  `content` text NOT NULL COMMENT '下载详细描述',
  `template` varchar(100) NOT NULL DEFAULT '' COMMENT '详情页显示模板',
  `file_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件ID',
  `download` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '下载次数',
  `size` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文档模型下载表';


DROP TABLE IF EXISTS `ke_favor`;
CREATE TABLE `ke_favor` (
  `fid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '收藏id',
  `goods_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '店铺id',
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '收藏人id',
  PRIMARY KEY (`fid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ke_file`;
CREATE TABLE `ke_file` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文件ID',
  `name` char(30) NOT NULL DEFAULT '' COMMENT '原始文件名',
  `savename` char(20) NOT NULL DEFAULT '' COMMENT '保存名称',
  `savepath` char(30) NOT NULL DEFAULT '' COMMENT '文件保存路径',
  `ext` char(5) NOT NULL DEFAULT '' COMMENT '文件后缀',
  `mime` char(40) NOT NULL DEFAULT '' COMMENT '文件mime类型',
  `size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `md5` char(32) NOT NULL DEFAULT '' COMMENT '文件md5',
  `sha1` char(40) NOT NULL DEFAULT '' COMMENT '文件 sha1编码',
  `location` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '文件保存位置',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '远程地址',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上传时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_md5` (`md5`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文件表';


DROP TABLE IF EXISTS `ke_goods`;
CREATE TABLE `ke_goods` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `num_iid` varchar(20) NOT NULL DEFAULT '0',
  `title` varchar(200) NOT NULL DEFAULT '' COMMENT '宝贝名称',
  `cate_id` int(10) unsigned NOT NULL DEFAULT '0',
  `tid` int(10) unsigned NOT NULL DEFAULT '0',
  `goods_type` varchar(255) NOT NULL COMMENT '商品类型',
  `provcity` varchar(20) NOT NULL DEFAULT '',
  `nick` varchar(32) NOT NULL DEFAULT '' COMMENT '卖家名称',
  `add_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '添加人',
  `add_uname` varchar(30) NOT NULL DEFAULT '',
  `seller_credit` varchar(32) NOT NULL DEFAULT '' COMMENT '卖家信用',
  `price` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT '现价',
  `market_price` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT '市场价',
  `volume` int(10) NOT NULL DEFAULT '0' COMMENT '30天推广数量',
  `comment_count` int(10) NOT NULL,
  `pic_url` varchar(255) NOT NULL COMMENT '图片地址',
  `item_url` varchar(255) NOT NULL DEFAULT '' COMMENT '宝贝地址',
  `click_url` text COMMENT '推广地址',
  `shop_url` text NOT NULL COMMENT '店铺地址',
  `sclick_url` text NOT NULL COMMENT '店铺推广链接',
  `item_body` longtext NOT NULL COMMENT '宝贝内容',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态，1审核通过，0未审核',
  `seo_title` varchar(255) DEFAULT '' COMMENT 'seo标题',
  `seo_keywords` varchar(255) DEFAULT '' COMMENT 'seo关键字',
  `seo_description` varchar(255) DEFAULT '',
  `ctime` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间（时间戳）',
  `hits` int(11) NOT NULL DEFAULT '0' COMMENT '商品点击数',
  `favor` int(11) NOT NULL DEFAULT '0' COMMENT '商品赞数',
  `sort` smallint(3) unsigned NOT NULL DEFAULT '0',
  `tag` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `goods_id` (`id`),
  KEY `num_iid` (`num_iid`)
) ENGINE=MyISAM AUTO_INCREMENT=481 DEFAULT CHARSET=utf8;

INSERT INTO `ke_goods` VALUES ('408', '520204184520', '2015新款刺绣印花连衣裙夏季女装韩版针织淑女两件套装裙子大摆裙', '2', '0', 'taobao', '', '', '1', 'admin', '', '198.00', '899.00', '0', '0', 'http://img4.tbcdn.cn/tfscom/i4/TB1wOpyIFXXXXbKXVXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=520204184520', null, '', '', '', '1', '', '', '', '1438935383', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('409', '38624398489', '斯黛欧2015夏季新款韩版潮宽松大码女装显瘦雪纺连衣裙胖MM1590', '2', '0', 'taobao', '', '', '1', 'admin', '', '128.00', '198.00', '0', '0', 'http://img2.tbcdn.cn/tfscom/i1/T1ocpNFJlbXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=38624398489', null, '', '', '', '1', '', '', '', '1438935383', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('410', '44694416919', '千羽斐无袖蕾丝雪纺连衣裙夏女装2015新款韩版修身显瘦白色裙子潮', '2', '0', 'taobao', '', '', '1', 'admin', '', '139.00', '239.00', '0', '0', 'http://img2.tbcdn.cn/tfscom/i3/TB1u83LHpXXXXa5XXXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=44694416919', null, '', '', '', '1', '', '', '', '1438935383', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('411', '45681133007', '2015新款夏季女装宽松显瘦背带裙连衣裙两件套装中长款吊带裙子潮', '2', '0', 'taobao', '', '', '1', 'admin', '', '138.00', '399.00', '0', '0', 'http://img2.tbcdn.cn/tfscom/i3/TB1GGFPHVXXXXbNXXXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=45681133007', null, '', '', '', '1', '', '', '', '1438935383', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('412', '44953146908', '真丝连衣裙夏季2015新款女装短袖拼接圆点高档桑蚕丝真丝绸缎裙子', '2', '0', 'taobao', '', '', '1', 'admin', '', '378.00', '718.00', '0', '0', 'http://img3.tbcdn.cn/tfscom/i3/TB1Z_yxHFXXXXX1XFXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=44953146908', null, '', '', '', '1', '', '', '', '1438935383', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('413', '520271059295', '欧洲站2015夏装新款韩版女装 名媛时尚百褶 修身显瘦雪纺连衣裙女', '2', '0', 'taobao', '', '', '1', 'admin', '', '148.00', '298.00', '0', '0', 'http://img2.tbcdn.cn/tfscom/i3/TB1Dyg4IpXXXXbqXVXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=520271059295', null, '', '', '', '1', '', '', '', '1438935383', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('414', '45742522599', '2015夏装新款针织红色棉麻连衣裙女装大码宽松休闲亚麻薄款中裙子', '2', '0', 'taobao', '', '', '1', 'admin', '', '118.00', '298.00', '0', '0', 'http://img3.tbcdn.cn/tfscom/i2/TB1mYaWIXXXXXX5XXXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=45742522599', null, '', '', '', '1', '', '', '', '1438935383', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('415', '520401288290', '欧洲站2015夏装新品韩版女装雪纺短袖中长款显瘦大码两件套连衣裙', '2', '0', 'taobao', '', '', '1', 'admin', '', '158.00', '399.00', '0', '0', 'http://img4.tbcdn.cn/tfscom/i2/TB1oKDvIVXXXXXXXpXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=520401288290', null, '', '', '', '1', '', '', '', '1438935383', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('416', '45386572729', '欧洲站名媛气质 棉麻连衣裙夏中裙2015女装新款韩版修身显瘦裙子', '2', '0', 'taobao', '', '', '1', 'admin', '', '118.00', '230.00', '0', '0', 'http://img4.tbcdn.cn/tfscom/i2/TB1uTYDHVXXXXcFXVXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=45386572729', null, '', '', '', '1', '', '', '', '1438935383', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('417', '520515723432', '欧洲站2015夏季女装新款短袖中长款雪纺连衣裙显瘦大码印花女裙子', '2', '0', 'taobao', '', '', '1', 'admin', '', '138.00', '888.00', '0', '0', 'http://img3.tbcdn.cn/tfscom/i3/TB1N64OIFXXXXbkXXXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=520515723432', null, '', '', '', '1', '', '', '', '1438935383', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('418', '21589959991', '烟花烫ZG2015夏新款女装甜美绣花蕾丝短袖雪纺印花连衣裙 柔夏', '2', '0', 'taobao', '', '', '1', 'admin', '', '268.00', '656.00', '0', '0', 'http://img1.tbcdn.cn/tfscom/i1/TB1bKQsHpXXXXaCXpXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=21589959991', null, '', '', '', '1', '', '', '', '1438935391', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('419', '520069660739', '欧洲站2015夏季新款女装中长款气质连衣裙欧根纱蓬蓬A字裙两件套', '2', '0', 'taobao', '', '', '1', 'admin', '', '148.00', '299.00', '0', '0', 'http://img4.tbcdn.cn/tfscom/i3/TB129MvIXXXXXcbXVXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=520069660739', null, '', '', '', '1', '', '', '', '1438935391', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('420', '520593467479', '2015夏韩版新款女装欧根纱蓬蓬裙无袖连衣裙修身荷叶边短裙背心裙', '2', '0', 'taobao', '', '', '1', 'admin', '', '138.00', '528.00', '0', '0', 'http://img3.tbcdn.cn/tfscom/i3/TB1vT9eHpXXXXcWapXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=520593467479', null, '', '', '', '1', '', '', '', '1438935391', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('421', '44462859192', '夏季女装蕾丝拼接雪纺连衣裙收腰显瘦礼服裙OL职业工作服包臀裙', '2', '0', 'taobao', '', '', '1', 'admin', '', '111.00', '259.00', '0', '0', 'http://img2.tbcdn.cn/tfscom/i3/TB1DBOqIVXXXXa1aFXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=44462859192', null, '', '', '', '1', '', '', '', '1438935391', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('422', '520305545531', '布伊格夏装2015韩版女装高端刺绣欧根纱蓬蓬裙套装裙两件套连衣裙', '2', '0', 'taobao', '', '', '1', 'admin', '', '199.00', '549.00', '0', '0', 'http://img2.tbcdn.cn/tfscom/i3/TB19v96IpXXXXctXVXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=520305545531', null, '', '', '', '1', '', '', '', '1438935391', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('423', '520712743447', '钻姿2015夏装新款女装潮长裙韩版复古文艺刺绣无袖棉麻连衣裙女夏', '2', '0', 'taobao', '', '', '1', 'admin', '', '168.00', '198.00', '0', '0', 'http://img4.tbcdn.cn/tfscom/i2/TB1JEHyIFXXXXaAXFXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=520712743447', null, '', '', '', '1', '', '', '', '1438935391', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('424', '520520082885', '2015新款针织印花连衣裙夏季韩版女装修身两件套装裙子淑女A字裙', '2', '0', 'taobao', '', '', '1', 'admin', '', '168.00', '799.00', '0', '0', 'http://img2.tbcdn.cn/tfscom/i2/TB1ZRguIFXXXXciXFXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=520520082885', null, '', '', '', '1', '', '', '', '1438935391', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('425', '37805958933', '琳丹乐夏装新品大码女装韩版蕾丝拼接雪纺碎花显瘦 修身连衣裙子', '2', '0', 'taobao', '', '', '1', 'admin', '', '159.00', '288.00', '0', '0', 'http://img1.tbcdn.cn/tfscom/i2/T1F9WIFrleXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=37805958933', null, '', '', '', '1', '', '', '', '1438935391', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('426', '520293865473', '卡洛莱时尚连衣裙夏 2015夏季新款女装修身显瘦气质雪纺连衣裙女', '2', '0', 'taobao', '', '', '1', 'admin', '', '168.00', '308.00', '0', '0', 'http://img2.tbcdn.cn/tfscom/i3/TB1Sv6IIpXXXXczXVXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=520293865473', null, '', '', '', '1', '', '', '', '1438935391', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('427', '520326664411', '2015夏装新款韩版大码女装气质淑女修身长裙夏季真丝皱雪纺连衣裙', '2', '0', 'taobao', '', '', '1', 'admin', '', '138.00', '276.00', '0', '0', 'http://img2.tbcdn.cn/tfscom/i2/TB1e6ntIpXXXXc8XVXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=520326664411', null, '', '', '', '1', '', '', '', '1438935391', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('428', '45077492685', '欧根纱连衣裙女夏季2015夏装女装新款韩版潮欧洲站蕾丝刺绣裙子', '2', '0', 'taobao', '', '', '1', 'admin', '', '228.00', '449.00', '0', '0', 'http://img4.tbcdn.cn/tfscom/i2/TB1xNwHHpXXXXXQapXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=45077492685', null, '', '', '', '1', '', '', '', '1438935402', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('429', '520298630806', '欧洲站2015夏季新款镶钻腰带雪纺褶皱修身显瘦中长款连衣裙女装潮', '2', '0', 'taobao', '', '', '1', 'admin', '', '169.00', '338.00', '0', '0', 'http://img4.tbcdn.cn/tfscom/i3/TB1IgguIpXXXXa0XFXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=520298630806', null, '', '', '', '1', '', '', '', '1438935402', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('430', '520707961074', '2015夏新款名媛女装短袖v领真丝雪纺连衣裙中裙修身韩版印花裙女', '2', '0', 'taobao', '', '', '1', 'admin', '', '168.00', '488.00', '0', '0', 'http://img4.tbcdn.cn/tfscom/i3/TB1RFARIFXXXXXdXpXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=520707961074', null, '', '', '', '1', '', '', '', '1438935402', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('431', '520610922412', '欧洲站2015夏装新款女装韩版修身雪纺裙显瘦收腰中裙短袖连衣裙夏', '2', '0', 'taobao', '', '', '1', 'admin', '', '168.00', '336.00', '0', '0', 'http://img1.tbcdn.cn/tfscom/i4/TB10DeJIFXXXXbfXXXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=520610922412', null, '', '', '', '1', '', '', '', '1438935402', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('432', '43567408687', '小黑裙2015夏季新款韩版修身显瘦无袖背心短裙雪纺A字连衣裙女装', '2', '0', 'taobao', '', '', '1', 'admin', '', '119.00', '399.00', '0', '0', 'http://img3.tbcdn.cn/tfscom/i1/TB1Gh7kHpXXXXc3XFXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=43567408687', null, '', '', '', '1', '', '', '', '1438935402', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('433', '520460332917', '韩国2015夏季女装新款气质修身显瘦棉麻连衣裙长款不规则一步裙夏', '2', '0', 'taobao', '', '', '1', 'admin', '', '168.00', '368.00', '0', '0', 'http://img2.tbcdn.cn/tfscom/i2/TB14bBdIFXXXXbnXXXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=520460332917', null, '', '', '', '1', '', '', '', '1438935402', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('434', '520214039677', '欧洲站2015夏季女装新款真丝印花短袖套装裙两件套短裙连衣裙女潮', '2', '0', 'taobao', '', '', '1', 'admin', '', '168.00', '238.00', '0', '0', 'http://img1.tbcdn.cn/tfscom/i1/TB1lQ0GIVXXXXcGXpXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=520214039677', null, '', '', '', '1', '', '', '', '1438935402', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('435', '520211095871', '2015夏装新款女装显瘦长裙夏季淑女气质长款韩版雪纺连衣裙女 夏', '2', '0', 'taobao', '', '', '1', 'admin', '', '158.00', '680.00', '0', '0', 'http://img4.tbcdn.cn/tfscom/i3/TB1sw8fIFXXXXXlaXXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=520211095871', null, '', '', '', '1', '', '', '', '1438935402', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('436', '520635131814', '2015夏季新款韩版通勤女装圆领短袖气质修身ol连衣裙女中裙淑女裙', '2', '0', 'taobao', '', '', '1', 'admin', '', '158.00', '468.00', '0', '0', 'http://img3.tbcdn.cn/tfscom/i1/TB1ogHdIFXXXXX3aXXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=520635131814', null, '', '', '', '1', '', '', '', '1438935402', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('438', '45557292713', '2015夏季男士短袖T恤纯棉骷髅头印花圆领修身体恤韩版男装半袖潮', '1', '0', 'taobao', '', '', '1', 'admin', '', '29.90', '99.00', '0', '0', 'http://img3.tbcdn.cn/tfscom/i2/TB1qxdvIpXXXXcUXXXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=45557292713', null, '', '', '', '1', '', '', '', '1438935497', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('439', '44304679973', '2015夏季男装体恤衫韩版修身型夏装潮男士大码休闲V领印花短袖T恤', '1', '0', 'taobao', '', '', '1', 'admin', '', '39.00', '168.00', '0', '0', 'http://img4.tbcdn.cn/tfscom/i4/TB1qhiIHpXXXXX6XFXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=44304679973', null, '', '', '', '1', '', '', '', '1438935497', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('440', '37496714009', '男士短袖T恤韩版夏季纯棉印花圆领修身体恤 青春男装半袖潮上衣服', '1', '0', 'taobao', '', '', '1', 'admin', '', '19.90', '98.00', '0', '0', 'http://img1.tbcdn.cn/tfscom/i1/TB1JNSyFFXXXXX4apXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=37496714009', null, '', '', '', '1', '', '', '', '1438935497', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('441', '45197266302', '2015夏季t恤男士短袖韩版修身V领纯棉半袖衣服潮男装学生印花t恤', '1', '0', 'taobao', '', '', '1', 'admin', '', '39.00', '199.00', '0', '0', 'http://img4.tbcdn.cn/tfscom/i3/TB1WxNQHFXXXXcTapXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=45197266302', null, '', '', '', '1', '', '', '', '1438935497', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('442', '37740461472', '夏装中年男士短袖t恤衫 翻领polo中老年大码男装上衣服夏季爸爸装', '1', '0', 'taobao', '', '', '1', 'admin', '', '65.00', '168.00', '0', '0', 'http://img4.tbcdn.cn/tfscom/i3/TB14W6uGVXXXXauapXXXXXXXXXX_!!1-item_pic.gif', 'http://item.taobao.com/item.htm?id=37740461472', null, '', '', '', '1', '', '', '', '1438935497', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('443', '44000862263', '2015夏季个性卡通短袖T恤男士日系潮男印花半袖韩版修身潮男装', '1', '0', 'taobao', '', '', '1', 'admin', '', '25.00', '99.00', '0', '0', 'http://img4.tbcdn.cn/tfscom/i4/TB1_ouEHFXXXXcYaXXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=44000862263', null, '', '', '', '1', '', '', '', '1438935497', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('444', '45594870813', '夏季T恤男士短袖潮男装青春日系修身圆领泼墨印花t恤半袖休闲体恤', '1', '0', 'taobao', '', '', '1', 'admin', '', '34.00', '98.00', '0', '0', 'http://img1.tbcdn.cn/tfscom/i1/TB161nZIXXXXXcZXpXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=45594870813', null, '', '', '', '1', '', '', '', '1438935497', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('445', '44189230799', '新款夏季男装加肥短袖男加大码胖子t恤宽松男装纯棉运动大号体恤', '1', '0', 'taobao', '', '', '1', 'admin', '', '18.00', '78.00', '0', '0', 'http://img4.tbcdn.cn/tfscom/i1/TB157RPHpXXXXXhXFXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=44189230799', null, '', '', '', '1', '', '', '', '1438935497', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('446', '44808393678', '男士短袖t恤夏季韩版修身半袖体恤衫潮印花圆领t桖男装夏天上衣服', '1', '0', 'taobao', '', '', '1', 'admin', '', '31.00', '79.00', '0', '0', 'http://img2.tbcdn.cn/tfscom/i2/TB1vwLjHVXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=44808393678', null, '', '', '', '1', '', '', '', '1438935497', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('447', '520184615372', 'JVR 2015韩版男装夏季新款青春男士短袖T恤圆领体恤打底衫修身潮', '1', '0', 'taobao', '', '', '1', 'admin', '', '15.80', '19.70', '0', '0', 'http://img2.tbcdn.cn/tfscom/i4/TB1xgHEHFXXXXbXaXXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=520184615372', null, '', '', '', '1', '', '', '', '1438935497', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('448', '43447640699', '2015夏季短袖t恤男韩版修身衣服男士短袖t恤夏季男装印花半袖T恤', '1', '0', 'taobao', '', '', '1', 'admin', '', '8.90', '49.00', '0', '0', 'http://img2.tbcdn.cn/tfscom/i2/TB1syhrIpXXXXc1XXXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=43447640699', null, '', '', '', '1', '', '', '', '1438935504', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('449', '37242727611', '夏季圆领印花T恤 男 男士短袖T恤纯棉青少年学生青春男装潮上衣服', '1', '0', 'taobao', '', '', '1', 'admin', '', '19.99', '98.00', '0', '0', 'http://img4.tbcdn.cn/tfscom/i3/TB1pbtoIXXXXXagXFXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=37242727611', null, '', '', '', '1', '', '', '', '1438935504', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('450', '37301356780', '男士短袖T恤韩版潮2015夏季纯棉印花圆领修身青春男装男人夏装', '1', '0', 'taobao', '', '', '1', 'admin', '', '19.80', '98.00', '0', '0', 'http://img2.tbcdn.cn/tfscom/i3/TB1mOJ5HFXXXXcVXVXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=37301356780', null, '', '', '', '1', '', '', '', '1438935504', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('451', '44367734237', '夏季新款男士短袖T恤 潮男装V领大码体恤韩版修身印花半袖男衣服', '1', '0', 'taobao', '', '', '1', 'admin', '', '29.90', '178.00', '0', '0', 'http://img4.tbcdn.cn/tfscom/i4/TB1OXjbHpXXXXXpXXXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=44367734237', null, '', '', '', '1', '', '', '', '1438935504', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('452', '44050311953', 'gusskater夏装新款男装T恤 男上衣潮 韩版印花男士短袖t恤 男半袖', '1', '0', 'taobao', '', '', '1', 'admin', '', '39.00', '139.00', '0', '0', 'http://img4.tbcdn.cn/tfscom/i4/TB1JAvzHFXXXXXfXpXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=44050311953', null, '', '', '', '1', '', '', '', '1438935504', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('453', '45630509713', '2015夏季男士短袖T恤日系修身V领半袖青少年休闲上衣打底衫潮男装', '1', '0', 'taobao', '', '', '1', 'admin', '', '26.56', '64.00', '0', '0', 'http://img3.tbcdn.cn/tfscom/i3/TB1M9boHVXXXXahaXXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=45630509713', null, '', '', '', '1', '', '', '', '1438935504', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('454', '37421933185', '特贝凡西短袖t恤男韩版修身半袖T恤男士纯色短袖男装夏季潮打底衫', '1', '0', 'taobao', '', '', '1', 'admin', '', '19.80', '99.00', '0', '0', 'http://img2.tbcdn.cn/tfscom/i4/TB1y0_OHpXXXXX4XpXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=37421933185', null, '', '', '', '1', '', '', '', '1438935504', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('456', '520887543216', '2015男装春秋新款男士卫衣韩版修身外套青年情侣学生运动服套装潮', '1', '0', 'taobao', '', '', '1', 'admin', '', '118.00', '398.00', '0', '0', 'http://img3.tbcdn.cn/tfscom/i1/TB1uuK.FVXXXXcCXpXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=520887543216', null, '', '', '', '1', '', '', '', '1438935504', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('457', '45803782317', '2015新款夏季日系复古男士短袖T恤半袖男装韩版夏天潮流圆领体恤', '1', '0', 'taobao', '', '', '1', 'admin', '', '19.80', '39.00', '0', '0', 'http://img2.tbcdn.cn/tfscom/i1/TB1xSpPIpXXXXXLXpXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=45803782317', null, '', '', '', '1', '', '', '', '1438935504', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('458', '23232608559', '特贝凡西短袖t恤男韩版修身衣服男士短袖t恤夏季男装印花半袖T恤', '1', '0', 'taobao', '', '', '1', 'admin', '', '19.80', '79.00', '0', '0', 'http://img2.tbcdn.cn/tfscom/i2/12867024482435985/T1tbSsXvXfXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=23232608559', null, '', '', '', '1', '', '', '', '1438935510', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('459', '44739041098', '男装短袖t恤套装大码T恤衫休闲夏季短裤韩版潮男式运动服2015夏新', '1', '0', 'taobao', '', '', '1', 'admin', '', '73.00', '158.00', '0', '0', 'http://img3.tbcdn.cn/tfscom/i4/TB1iJx1IVXXXXXaXXXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=44739041098', null, '', '', '', '1', '', '', '', '1438935510', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('460', '44208320792', '2015新款夏季春款男士短袖T恤半袖男装韩版夏天男式日系潮流圆领', '1', '0', 'taobao', '', '', '1', 'admin', '', '9.90', '98.00', '0', '0', 'http://img2.tbcdn.cn/tfscom/i4/TB1HeZ4GVXXXXb6aXXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=44208320792', null, '', '', '', '1', '', '', '', '1438935510', '0', '0', '0', '0');
INSERT INTO `ke_goods` VALUES ('461', '520652735334', '秋冬男士大码卫衣加绒套装韩版棒球服休闲运动新款外套开衫男装潮', '1', '0', 'taobao', '', '', '1', 'admin', '', '138.00', '263.00', '0', '0', 'http://img1.tbcdn.cn/tfscom/i2/TB1ElyWIFXXXXcCXFXXXXXXXXXX_!!0-item_pic.jpg', 'http://item.taobao.com/item.htm?id=520652735334', null, '', '', '', '1', '', '', '', '1438935510', '0', '0', '0', '0');


DROP TABLE IF EXISTS `ke_hooks`;
CREATE TABLE `ke_hooks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(40) NOT NULL DEFAULT '' COMMENT '钩子名称',
  `description` text NULL  COMMENT '描述',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '类型',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `addons` varchar(255) NOT NULL DEFAULT '' COMMENT '钩子挂载的插件 ''，''分割',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

INSERT INTO `ke_hooks` VALUES ('1', 'pageHeader', '页面header钩子，一般用于加载插件CSS文件和代码', '1', '0', '', '1');
INSERT INTO `ke_hooks` VALUES ('2', 'pageFooter', '页面footer钩子，一般用于加载插件JS文件和JS代码', '1', '0', 'ReturnTop', '1');
INSERT INTO `ke_hooks` VALUES ('3', 'documentEditForm', '添加编辑表单的 扩展内容钩子', '1', '0', 'Attachment', '1');
INSERT INTO `ke_hooks` VALUES ('4', 'documentDetailAfter', '文档末尾显示', '1', '0', 'Attachment,SocialComment', '1');
INSERT INTO `ke_hooks` VALUES ('5', 'documentDetailBefore', '页面内容前显示用钩子', '1', '0', '', '1');
INSERT INTO `ke_hooks` VALUES ('6', 'documentSaveComplete', '保存文档数据后的扩展钩子', '2', '0', 'Attachment', '1');
INSERT INTO `ke_hooks` VALUES ('7', 'documentEditFormContent', '添加编辑表单的内容显示钩子', '1', '0', 'Editor', '1');
INSERT INTO `ke_hooks` VALUES ('8', 'adminArticleEdit', '后台内容编辑页编辑器', '1', '1378982734', 'EditorForAdmin', '1');
INSERT INTO `ke_hooks` VALUES ('13', 'AdminIndex', '首页小格子个性化显示', '1', '1382596073', 'SiteStat,SystemInfo,DevTeam', '1');
INSERT INTO `ke_hooks` VALUES ('14', 'topicComment', '评论提交方式扩展钩子。', '1', '1380163518', 'Editor', '1');
INSERT INTO `ke_hooks` VALUES ('16', 'app_begin', '应用开始', '2', '1384481614', '', '1');

-- ----------------------------
-- Table structure for `ke_link`
-- ----------------------------

DROP TABLE IF EXISTS `ke_link`;
CREATE TABLE `ke_link` (
  `id` mediumint(5) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '标题',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '连接',
  `img` varchar(255) NOT NULL DEFAULT '' COMMENT '图片',
  `sort` mediumint(2) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '连接',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ke_member`;
CREATE TABLE `ke_member` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `nickname` char(16) NOT NULL DEFAULT '' COMMENT '昵称',
  `avatar` varchar(255) NOT NULL,
  `sex` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '性别',
  `birthday` date NOT NULL DEFAULT '0000-00-00' COMMENT '生日',
  `qq` char(10) NOT NULL DEFAULT '' COMMENT 'qq号',
  `score` mediumint(8) NOT NULL DEFAULT '0' COMMENT '用户积分',
  `login` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '登录次数',
  `reg_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '注册IP',
  `reg_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '注册时间',
  `last_login_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后登录IP',
  `last_login_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '会员状态',
  PRIMARY KEY (`uid`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='会员表';


DROP TABLE IF EXISTS `ke_menu`;
CREATE TABLE `ke_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文档ID',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '标题',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上级分类ID',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序（同级有效）',
  `url` char(255) NOT NULL DEFAULT '' COMMENT '链接地址',
  `hide` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否隐藏',
  `tip` varchar(255) NOT NULL DEFAULT '' COMMENT '提示',
  `group` varchar(50) DEFAULT '' COMMENT '分组',
  `is_dev` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否仅开发者模式可见',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=150 DEFAULT CHARSET=utf8;


INSERT INTO `ke_menu` VALUES ('1', '首页', '0', '1', 'Index/index', '0', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('2', '内容', '0', '2', 'Article/index', '0', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('3', '文档列表', '2', '0', 'article/index', '1', '', '内容', '0','1');
INSERT INTO `ke_menu` VALUES ('4', '新增', '3', '0', 'article/add', '0', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('5', '编辑', '3', '0', 'article/edit', '0', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('6', '改变状态', '3', '0', 'article/setStatus', '0', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('7', '保存', '3', '0', 'article/update', '0', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('8', '保存草稿', '3', '0', 'article/autoSave', '0', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('9', '移动', '3', '0', 'article/move', '0', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('10', '复制', '3', '0', 'article/copy', '0', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('11', '粘贴', '3', '0', 'article/paste', '0', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('12', '导入', '3', '0', 'article/batchOperate', '0', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('13', '回收站', '2', '0', 'article/recycle', '1', '', '内容', '0','1');
INSERT INTO `ke_menu` VALUES ('14', '还原', '13', '0', 'article/permit', '0', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('15', '清空', '13', '0', 'article/clear', '0', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('16', '用户', '0', '3', 'User/index', '0', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('17', '用户信息', '16', '0', 'User/index', '0', '', '用户管理', '0','1');
INSERT INTO `ke_menu` VALUES ('18', '新增用户', '17', '0', 'User/add', '0', '添加新用户', '', '0','1');
INSERT INTO `ke_menu` VALUES ('19', '用户行为', '16', '0', 'User/action', '0', '', '行为管理', '0','1');
INSERT INTO `ke_menu` VALUES ('20', '新增用户行为', '19', '0', 'User/addaction', '0', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('21', '编辑用户行为', '19', '0', 'User/editaction', '0', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('22', '保存用户行为', '19', '0', 'User/saveAction', '0', '\"用户->用户行为\"保存编辑和新增的用户行为', '', '0','1');
INSERT INTO `ke_menu` VALUES ('23', '变更行为状态', '19', '0', 'User/setStatus', '0', '\"用户->用户行为\"中的启用,禁用和删除权限', '', '0','1');
INSERT INTO `ke_menu` VALUES ('24', '禁用会员', '19', '0', 'User/changeStatus?method=forbidUser', '0', '\"用户->用户信息\"中的禁用', '', '0','1');
INSERT INTO `ke_menu` VALUES ('25', '启用会员', '19', '0', 'User/changeStatus?method=resumeUser', '0', '\"用户->用户信息\"中的启用', '', '0','1');
INSERT INTO `ke_menu` VALUES ('26', '删除会员', '19', '0', 'User/changeStatus?method=deleteUser', '0', '\"用户->用户信息\"中的删除', '', '0','1');
INSERT INTO `ke_menu` VALUES ('27', '权限管理', '16', '0', 'AuthManager/index', '0', '', '用户管理', '0','1');
INSERT INTO `ke_menu` VALUES ('28', '删除', '27', '0', 'AuthManager/changeStatus?method=deleteGroup', '0', '删除用户组', '', '0','1');
INSERT INTO `ke_menu` VALUES ('29', '禁用', '27', '0', 'AuthManager/changeStatus?method=forbidGroup', '0', '禁用用户组', '', '0','1');
INSERT INTO `ke_menu` VALUES ('30', '恢复', '27', '0', 'AuthManager/changeStatus?method=resumeGroup', '0', '恢复已禁用的用户组', '', '0','1');
INSERT INTO `ke_menu` VALUES ('31', '新增', '27', '0', 'AuthManager/createGroup', '0', '创建新的用户组', '', '0','1');
INSERT INTO `ke_menu` VALUES ('32', '编辑', '27', '0', 'AuthManager/editGroup', '0', '编辑用户组名称和描述', '', '0','1');
INSERT INTO `ke_menu` VALUES ('33', '保存用户组', '27', '0', 'AuthManager/writeGroup', '0', '新增和编辑用户组的\"保存\"按钮', '', '0','1');
INSERT INTO `ke_menu` VALUES ('34', '授权', '27', '0', 'AuthManager/group', '0', '\"后台 \\ 用户 \\ 用户信息\"列表页的\"授权\"操作按钮,用于设置用户所属用户组', '', '0','1');
INSERT INTO `ke_menu` VALUES ('35', '访问授权', '27', '0', 'AuthManager/access', '0', '\"后台 \\ 用户 \\ 权限管理\"列表页的\"访问授权\"操作按钮', '', '0','1');
INSERT INTO `ke_menu` VALUES ('36', '成员授权', '27', '0', 'AuthManager/user', '0', '\"后台 \\ 用户 \\ 权限管理\"列表页的\"成员授权\"操作按钮', '', '0','1');
INSERT INTO `ke_menu` VALUES ('37', '解除授权', '27', '0', 'AuthManager/removeFromGroup', '0', '\"成员授权\"列表页内的解除授权操作按钮', '', '0','1');
INSERT INTO `ke_menu` VALUES ('38', '保存成员授权', '27', '0', 'AuthManager/addToGroup', '0', '\"用户信息\"列表页\"授权\"时的\"保存\"按钮和\"成员授权\"里右上角的\"添加\"按钮)', '', '0','1');
INSERT INTO `ke_menu` VALUES ('39', '分类授权', '27', '0', 'AuthManager/category', '0', '\"后台 \\ 用户 \\ 权限管理\"列表页的\"分类授权\"操作按钮', '', '0','1');
INSERT INTO `ke_menu` VALUES ('40', '保存分类授权', '27', '0', 'AuthManager/addToCategory', '0', '\"分类授权\"页面的\"保存\"按钮', '', '0','1');
INSERT INTO `ke_menu` VALUES ('41', '模型授权', '27', '0', 'AuthManager/modelauth', '0', '\"后台 \\ 用户 \\ 权限管理\"列表页的\"模型授权\"操作按钮', '', '0','1');
INSERT INTO `ke_menu` VALUES ('42', '保存模型授权', '27', '0', 'AuthManager/addToModel', '0', '\"分类授权\"页面的\"保存\"按钮', '', '0','1');
INSERT INTO `ke_menu` VALUES ('43', '扩展', '0', '7', 'Addons/index', '0', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('44', '插件管理', '43', '1', 'Addons/index', '0', '', '扩展', '0','1');
INSERT INTO `ke_menu` VALUES ('45', '创建', '44', '0', 'Addons/create', '0', '服务器上创建插件结构向导', '', '0','1');
INSERT INTO `ke_menu` VALUES ('46', '检测创建', '44', '0', 'Addons/checkForm', '0', '检测插件是否可以创建', '', '0','1');
INSERT INTO `ke_menu` VALUES ('47', '预览', '44', '0', 'Addons/preview', '0', '预览插件定义类文件', '', '0','1');
INSERT INTO `ke_menu` VALUES ('48', '快速生成插件', '44', '0', 'Addons/build', '0', '开始生成插件结构', '', '0','1');
INSERT INTO `ke_menu` VALUES ('49', '设置', '44', '0', 'Addons/config', '0', '设置插件配置', '', '0','1');
INSERT INTO `ke_menu` VALUES ('50', '禁用', '44', '0', 'Addons/disable', '0', '禁用插件', '', '0','1');
INSERT INTO `ke_menu` VALUES ('51', '启用', '44', '0', 'Addons/enable', '0', '启用插件', '', '0','1');
INSERT INTO `ke_menu` VALUES ('52', '安装', '44', '0', 'Addons/install', '0', '安装插件', '', '0','1');
INSERT INTO `ke_menu` VALUES ('53', '卸载', '44', '0', 'Addons/uninstall', '0', '卸载插件', '', '0','1');
INSERT INTO `ke_menu` VALUES ('54', '更新配置', '44', '0', 'Addons/saveconfig', '0', '更新插件配置处理', '', '0','1');
INSERT INTO `ke_menu` VALUES ('55', '插件后台列表', '44', '0', 'Addons/adminList', '0', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('56', 'URL方式访问插件', '44', '0', 'Addons/execute', '0', '控制是否有权限通过url访问插件控制器方法', '', '0','1');
INSERT INTO `ke_menu` VALUES ('57', '钩子管理', '43', '2', 'Addons/hooks', '0', '', '扩展', '0','1');
INSERT INTO `ke_menu` VALUES ('58', '模型管理', '68', '3', 'Model/index', '0', '', '系统设置', '0','1');
INSERT INTO `ke_menu` VALUES ('59', '新增', '58', '0', 'model/add', '0', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('60', '编辑', '58', '0', 'model/edit', '0', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('61', '改变状态', '58', '0', 'model/setStatus', '0', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('62', '保存数据', '58', '0', 'model/update', '0', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('63', '属性管理', '68', '0', 'Attribute/index', '1', '网站属性配置。', '', '0','1');
INSERT INTO `ke_menu` VALUES ('64', '新增', '63', '0', 'Attribute/add', '0', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('65', '编辑', '63', '0', 'Attribute/edit', '0', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('66', '改变状态', '63', '0', 'Attribute/setStatus', '0', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('67', '保存数据', '63', '0', 'Attribute/update', '0', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('68', '系统', '0', '4', 'Config/group', '0', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('69', '网站设置', '68', '1', 'Config/group', '0', '', '系统设置', '0','1');
INSERT INTO `ke_menu` VALUES ('70', '配置管理', '68', '4', 'Config/index', '0', '', '系统设置', '0','1');
INSERT INTO `ke_menu` VALUES ('71', '编辑', '70', '0', 'Config/edit', '0', '新增编辑和保存配置', '', '0','1');
INSERT INTO `ke_menu` VALUES ('72', '删除', '70', '0', 'Config/del', '0', '删除配置', '', '0','1');
INSERT INTO `ke_menu` VALUES ('73', '新增', '70', '0', 'Config/add', '0', '新增配置', '', '0','1');
INSERT INTO `ke_menu` VALUES ('74', '保存', '70', '0', 'Config/save', '0', '保存配置', '', '0','1');
INSERT INTO `ke_menu` VALUES ('75', '菜单管理', '68', '5', 'Menu/index', '0', '', '系统设置', '0','1');
INSERT INTO `ke_menu` VALUES ('76', '导航管理', '68', '6', 'Channel/index', '0', '', '系统设置', '0','1');
INSERT INTO `ke_menu` VALUES ('77', '新增', '76', '0', 'Channel/add', '0', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('78', '编辑', '76', '0', 'Channel/edit', '0', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('79', '删除', '76', '0', 'Channel/del', '0', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('80', '分类管理', '68', '2', 'Category/index', '0', '', '系统设置', '0','1');
INSERT INTO `ke_menu` VALUES ('81', '编辑', '80', '0', 'Category/edit', '0', '编辑和保存栏目分类', '', '0','1');
INSERT INTO `ke_menu` VALUES ('82', '新增', '80', '0', 'Category/add', '0', '新增栏目分类', '', '0','1');
INSERT INTO `ke_menu` VALUES ('83', '删除', '80', '0', 'Category/remove', '0', '删除栏目分类', '', '0','1');
INSERT INTO `ke_menu` VALUES ('84', '移动', '80', '0', 'Category/operate/type/move', '0', '移动栏目分类', '', '0','1');
INSERT INTO `ke_menu` VALUES ('85', '合并', '80', '0', 'Category/operate/type/merge', '0', '合并栏目分类', '', '0','1');
INSERT INTO `ke_menu` VALUES ('86', '备份数据库', '68', '0', 'Database/index?type=export', '0', '', '数据备份', '0','1');
INSERT INTO `ke_menu` VALUES ('87', '备份', '86', '0', 'Database/export', '0', '备份数据库', '', '0','1');
INSERT INTO `ke_menu` VALUES ('88', '优化表', '86', '0', 'Database/optimize', '0', '优化数据表', '', '0','1');
INSERT INTO `ke_menu` VALUES ('89', '修复表', '86', '0', 'Database/repair', '0', '修复数据表', '', '0','1');
INSERT INTO `ke_menu` VALUES ('90', '还原数据库', '68', '0', 'Database/index?type=import', '0', '', '数据备份', '0','1');
INSERT INTO `ke_menu` VALUES ('91', '恢复', '90', '0', 'Database/import', '0', '数据库恢复', '', '0','1');
INSERT INTO `ke_menu` VALUES ('92', '删除', '90', '0', 'Database/del', '0', '删除备份文件', '', '0','1');
INSERT INTO `ke_menu` VALUES ('93', '其他', '0', '5', 'other', '1', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('96', '新增', '75', '0', 'Menu/add', '0', '', '系统设置', '0','1');
INSERT INTO `ke_menu` VALUES ('98', '编辑', '75', '0', 'Menu/edit', '0', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('106', '行为日志', '16', '0', 'Action/actionlog', '0', '', '行为管理', '0','1');
INSERT INTO `ke_menu` VALUES ('108', '修改密码', '16', '0', 'User/updatePassword', '1', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('109', '修改昵称', '16', '0', 'User/updateNickname', '1', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('110', '查看行为日志', '106', '0', 'action/edit', '1', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('112', '新增数据', '58', '0', 'think/add', '1', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('113', '编辑数据', '58', '0', 'think/edit', '1', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('114', '导入', '75', '0', 'Menu/import', '0', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('115', '生成', '58', '0', 'Model/generate', '0', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('116', '新增钩子', '57', '0', 'Addons/addHook', '0', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('117', '编辑钩子', '57', '0', 'Addons/edithook', '0', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('118', '文档排序', '3', '0', 'Article/sort', '1', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('119', '排序', '70', '0', 'Config/sort', '1', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('120', '排序', '75', '0', 'Menu/sort', '1', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('121', '排序', '76', '0', 'Channel/sort', '1', '', '', '0','1');




INSERT INTO `ke_menu` VALUES ('131', '商品', '0', '1', 'Goods/index', '0', '', '商品', '0','1');

INSERT INTO `ke_menu` VALUES ('122', '数据列表', '58', '0', 'think/lists', '1', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('132', '商品列表', '131', '0', 'Goods/index', '0', '', '商品', '0','1');

INSERT INTO `ke_menu` VALUES ('137', '商品采集', '131', '0', 'Collect/index', '0', '', '采集', '0','1');
INSERT INTO `ke_menu` VALUES ('134', '添加商品', '131', '1', 'Goods/edit', '0', '', '商品', '0','1');
INSERT INTO `ke_menu` VALUES ('123', '审核列表', '3', '0', 'Article/examine', '1', '', '', '0','1');
INSERT INTO `ke_menu` VALUES ('135', '分类列表', '131', '10', 'CategoryGoods/index', '0', '', '分类', '0','1');
INSERT INTO `ke_menu` VALUES ('139', '添加分类', '131', '0', 'CategoryGoods/add', '0', '', '分类', '0','1');
INSERT INTO `ke_menu` VALUES ('140', '采集到的商品', '131', '0', 'Collect/collectList', '0', '', '采集', '0','1');
INSERT INTO `ke_menu` VALUES ('141', '广告', '0', '6', 'Ad/index', '0', '', '广告', '0', '1');
INSERT INTO `ke_menu` VALUES ('142', '列表', '141', '0', 'Ad/index', '0', '', '广告', '0', '1');
INSERT INTO `ke_menu` VALUES ('143', '添加广告', '141', '0', 'Ad/edit', '0', '', '广告', '0', '1');
INSERT INTO `ke_menu` VALUES ('144', '标签管理', '68', '6', 'Tag/index', '0', '', '系统设置', '0', '1');
INSERT INTO `ke_menu` VALUES ('145', '添加标签', '144', '0', 'Tag/add', '1', '', '', '0', '1');
INSERT INTO `ke_menu` VALUES ('146', '专题列表', '131', '7', 'Topic/index', '0', '', '专题', '0', '1');
INSERT INTO `ke_menu` VALUES ('147', '添加专题', '131', '8', 'Topic/edit', '0', '', '专题', '0', '1');
INSERT INTO `ke_menu` VALUES ('148', '更新商品信息', '131', '0', 'Collect/update', '0', '', '采集', '0', '1');
INSERT INTO `ke_menu` VALUES ('149', '专题商品', '131', '0', 'Topic/topicGoods', '1', '', '专题', '0', '1');
DROP TABLE IF EXISTS `ke_model`;
CREATE TABLE `ke_model` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '模型ID',
  `name` char(30) NOT NULL DEFAULT '' COMMENT '模型标识',
  `title` char(30) NOT NULL DEFAULT '' COMMENT '模型名称',
  `extend` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '继承的模型',
  `relation` varchar(30) NOT NULL DEFAULT '' COMMENT '继承与被继承模型的关联字段',
  `need_pk` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '新建表时是否需要主键字段',
  `field_sort` text NULL  COMMENT '表单字段排序',
  `field_group` varchar(255) NOT NULL DEFAULT '1:基础' COMMENT '字段分组',
  `attribute_list` text NULL  COMMENT '属性列表（表的字段）',
  `attribute_alias` varchar(255) NOT NULL DEFAULT '' COMMENT '属性别名定义',
  `template_list` varchar(100) NOT NULL DEFAULT '' COMMENT '列表模板',
  `template_add` varchar(100) NOT NULL DEFAULT '' COMMENT '新增模板',
  `template_edit` varchar(100) NOT NULL DEFAULT '' COMMENT '编辑模板',
  `list_grid` text NULL  COMMENT '列表定义',
  `list_row` smallint(2) unsigned NOT NULL DEFAULT '10' COMMENT '列表数据长度',
  `search_key` varchar(50) NOT NULL DEFAULT '' COMMENT '默认搜索字段',
  `search_list` varchar(255) NOT NULL DEFAULT '' COMMENT '高级搜索的字段',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  `engine_type` varchar(25) NOT NULL DEFAULT 'MyISAM' COMMENT '数据库引擎',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='文档模型表';

INSERT INTO `ke_model` VALUES ('1', 'document', '基础文档', '0', '', '1', '{\"1\":[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"10\",\"11\",\"12\",\"13\",\"14\",\"15\",\"16\",\"17\",\"18\",\"19\",\"20\",\"21\",\"22\"]}', '1:基础', '', '','', '', '', 'id:编号\r\ntitle:标题:[EDIT]\r\ntype:类型\r\nupdate_time:最后更新\r\nstatus:状态\r\nview:浏览\r\nid:操作:[EDIT]|编辑,[DELETE]|删除', '0', '', '', '1383891233', '1384507827', '1', 'MyISAM');
INSERT INTO `ke_model` VALUES ('2', 'article', '文章', '1', '', '1', '{\"1\":[\"3\",\"24\",\"2\",\"5\"],\"2\":[\"9\",\"13\",\"19\",\"10\",\"12\",\"16\",\"17\",\"26\",\"20\",\"14\",\"11\",\"25\"]}', '1:基础,2:扩展', '','', '', '', '', '', '0', '', '', '1383891243', '1387260622', '1', 'MyISAM');
INSERT INTO `ke_model` VALUES ('3', 'download', '下载', '1', '', '1', '{\"1\":[\"3\",\"28\",\"30\",\"32\",\"2\",\"5\",\"31\"],\"2\":[\"13\",\"10\",\"27\",\"9\",\"12\",\"16\",\"17\",\"19\",\"11\",\"20\",\"14\",\"29\"]}', '1:基础,2:扩展', '', '','', '', '', '', '0', '', '', '1383891252', '1387260449', '1', 'MyISAM');



DROP TABLE IF EXISTS `ke_picture`;
CREATE TABLE `ke_picture` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id自增',
  `path` varchar(255) NOT NULL DEFAULT '' COMMENT '路径',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '图片链接',
  `md5` char(32) NOT NULL DEFAULT '' COMMENT '文件md5',
  `sha1` char(40) NOT NULL DEFAULT '' COMMENT '文件 sha1编码',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
INSERT INTO `ke_picture` VALUES ('23', '/Uploads/goods_img/2015-08-14/55cd7eb766649.jpg', '', '2cc2ebc11a41f7f429c8effd78fd6430', 'b7b8b2d1c7ac22c5897de083568dfd66691f96d5', '1', '1439530679');
INSERT INTO `ke_picture` VALUES ('22', '/Uploads/Ad/2015-08-14/55cd762c5ecd5.jpg', '', 'c9b537433e1dffc7ee34479924930821', '1ae3ed8c6e8d6094d33eab42ff4324618bcc0573', '1', '1439528492');
INSERT INTO `ke_picture` VALUES ('21', '/Uploads/Ad/2015-08-14/55cd75fb1d2c3.jpg', '', '96fe982b313d3eeb550b83070ca29166', 'c8f4b9908dea7361df631c35f7d7dc40fffe1c7d', '1', '1439528443');
INSERT INTO `ke_picture` VALUES ('16', '/Uploads/goods_img/2015-08-14/55cd6f145743e.jpg', '', 'dfe20c75e45e93709f303714062eefc6', 'bfc23d65d7c3e083e97ec01aea87fccc190f0677', '1', '1439526676');
INSERT INTO `ke_picture` VALUES ('17', '/Uploads/ad_img/2015-08-14/55cd704d77785.jpg', '', 'dc567775e8c3d4a85786ed107015f78b', '6a242ac5531aa27f95ad0e444889ca4816b354b8', '1', '1439526989');
INSERT INTO `ke_picture` VALUES ('18', '/Uploads/Ad/2015-08-14/55cd70da5dd97.jpg', '', 'aca9c851429d0f4824780933a387c8f9', 'e90337d5d529eceed48a96e0890d8c23853800b4', '1', '1439527130');
INSERT INTO `ke_picture` VALUES ('19', '/Uploads/Ad/2015-08-14/55cd7108f0fc1.jpg', '', '186c3f07af78e3cebe082a6f60dedef3', '1a92a1339cfc252989b4be539d5526aa94c09103', '1', '1439527176');
INSERT INTO `ke_picture` VALUES ('20', '/Uploads/Ad/2015-08-14/55cd749348f9f.jpg', '', '0443329a8557c715686e48c217773ed4', '23101ddde6a1ecb9b83d231602c92ad61f9be53c', '1', '1439528083');
INSERT INTO `ke_picture` VALUES ('24', '/Uploads/goods_img/2015-08-14/55cd96ea0fcca.jpg', '', '0ea0328df924cc0bc27b46faa7ddbc76', '8e7cf41ab7713787115fa1d6931734eb88a66b76', '1', '1439536874');
INSERT INTO `ke_picture` VALUES ('25', '/Uploads/goods_img/2015-08-14/55cda15e6c1aa.jpg', '', '5fad0c0eb5c98266f9c54a78a1ca56db', 'feb9bf75138af57ce65a4a1097f78190a57db4fb', '1', '1439539550');
INSERT INTO `ke_picture` VALUES ('26', '/Uploads/Ad/2015-08-16/55cffad37a81c.jpg', '', 'f093e95dfa0cfb7df88e03dc3353a20a', 'ad690509004a850b935a4ba5c036affdf31dd815', '1', '1439693523');
INSERT INTO `ke_picture` VALUES ('27', '/Uploads/Ad/2015-08-16/55d01f4264678.jpg', '', 'a77dc605fd2ccb7bd6b9231467265b78', 'b84bdfdd97830b9529c2bb58b31507c1a591d3f5', '1', '1439702850');
INSERT INTO `ke_picture` VALUES ('28', '/Uploads/Ad/2015-08-16/55d02077df9f7.png', '', 'c6d369eedf0c55dbcda4ee4668b8829b', '80ed5bc5775b72ddd40f73e0bfc1377f9bb227f1', '1', '1439703159');
INSERT INTO `ke_picture` VALUES ('29', '/Uploads/goods_img/2015-08-27/55ded24080d9f.jpg', '', '856e745306131ff7751877a2c9824f51', '1af3d2e25156cd2a849b565d9d816c11bac9ab3f', '1', '1440666176');
INSERT INTO `ke_picture` VALUES ('30', '/Uploads/Ad/2015-09-02/55e66405ee82c.png', '', '27588d010933a13e9dd2167435c2fc7a', '038cb3b1e52de5265ac90cdbc5da18c852987f30', '1', '1441162245');
INSERT INTO `ke_picture` VALUES ('31', '/Uploads/Ad/2015-09-02/55e6ae67f3f70.jpg', '', '609a1f9b3769897cd400b038616e7af7', 'fa704922fadfe97c58e1d625a45e94c62b6b6b2b', '1', '1441181287');
INSERT INTO `ke_picture` VALUES ('32', '/Uploads/Ad/2015-09-02/55e6ae75617c2.jpg', '', 'b5ff344019e5753e9dfa511e9bf1f10c', 'd465e58992e54ee26729d92976341667c427d06b', '1', '1441181301');
INSERT INTO `ke_picture` VALUES ('33', '/Uploads/Ad/2015-09-02/55e6aea037682.jpg', '', '1442a220445c619894e1c33b56422044', 'acbc1b1a033601a39ca622be653b96d0ac64b486', '1', '1441181344');
INSERT INTO `ke_picture` VALUES ('34', '/Uploads/Ad/2015-09-02/55e6b116c91fb.jpg', '', '3681b55af3bff83175c8a305b3d3c8fc', '80e749fb3854fb9850998aaf513296950a76deaf', '1', '1441181974');
INSERT INTO `ke_picture` VALUES ('35', '/Uploads/Ad/2015-09-02/55e6b187f0810.jpg', '', 'f550b45e849f775c51a42c2bf810134f', 'fcfeeee31be3700586ae02d5008d8f437f9bcc70', '1', '1441182087');
INSERT INTO `ke_picture` VALUES ('36', '/Uploads/goods_img/2015-09-02/55e6b316e7427.jpg', '', 'a1379cca7c32a0a990d30c148077ac07', '47f751d8df346aaf32f4beaedde9b150e54963f5', '1', '1441182486');
INSERT INTO `ke_picture` VALUES ('37', '/Uploads/goods_img/2015-09-02/55e6b3b78134f.jpg', '', 'fed03a1d1df5a7d1bb53b5b0557bfe83', 'bc644b4648df1f7e78508885acc9af15aa92ac6b', '1', '1441182647');
INSERT INTO `ke_picture` VALUES ('38', '/Uploads/goods_img/2015-09-02/55e6b426deb3d.jpg', '', '7500057d45203db214ab9d45006957f7', '4eed04d9da6cc2e7d137d857cd93b4341ef094c0', '1', '1441182758');
INSERT INTO `ke_picture` VALUES ('39', '/Uploads/goods_img/2015-09-02/55e6b4894020e.jpg', '', '5310fd63d37763837f6f11ecdc430a09', '70d0cb1f60a358c43ff43c5e6ad9f93fb66763ed', '1', '1441182857');

DROP TABLE IF EXISTS `ke_tag`;
CREATE TABLE `ke_tag` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
INSERT INTO `ke_tag` VALUES ('1', '女装', '1');
INSERT INTO `ke_tag` VALUES ('2', '厨房', '1');
INSERT INTO `ke_tag` VALUES ('3', '卧室', '1');
INSERT INTO `ke_tag` VALUES ('4', '办公室', '1');
INSERT INTO `ke_tag` VALUES ('5', '客厅', '1');
INSERT INTO `ke_tag` VALUES ('6', '创意', '1');
INSERT INTO `ke_tag` VALUES ('7', '摆件', '1');
INSERT INTO `ke_tag` VALUES ('8', '装修', '1');
INSERT INTO `ke_tag` VALUES ('9', '户外', '1');
INSERT INTO `ke_tag` VALUES ('10', '钓鱼', '1');
INSERT INTO `ke_tag` VALUES ('11', '运动', '1');
INSERT INTO `ke_tag` VALUES ('12', '桌面', '1');
INSERT INTO `ke_tag` VALUES ('13', '时尚', '1');
INSERT INTO `ke_tag` VALUES ('14', '数码', '1');
INSERT INTO `ke_tag` VALUES ('15', '健身', '1');
INSERT INTO `ke_tag` VALUES ('16', '有氧运动', '1');
INSERT INTO `ke_tag` VALUES ('17', '创意生活', '1');
INSERT INTO `ke_tag` VALUES ('18', '户外运动', '1');
INSERT INTO `ke_tag` VALUES ('19', '这是个比较长的标签用来占空间的', '1');
INSERT INTO `ke_tag` VALUES ('20', '这是个比较长的标签用来占空间的第二个', '1');
INSERT INTO `ke_tag` VALUES ('21', '沙发', '1');
INSERT INTO `ke_tag` VALUES ('22', '这是个比较长的标签用来占空间的迪桑', '1');
INSERT INTO `ke_tag` VALUES ('23', '茶几', '1');
INSERT INTO `ke_tag` VALUES ('24', '凳子', '1');
INSERT INTO `ke_tag` VALUES ('25', '电视机', '1');
DROP TABLE IF EXISTS `ke_topic`;
CREATE TABLE `ke_topic` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `topic_name` varchar(50) NOT NULL DEFAULT '',
  `icon` varchar(255) NOT NULL,
  `pic_url` varchar(255) NOT NULL DEFAULT '',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `discription` text NOT NULL,
  `background_color` varchar(10) NOT NULL DEFAULT '#fff',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ke_topic
INSERT INTO `ke_topic` VALUES ('2', '清凉一夏  ', '', '/Uploads/Ad/2015-09-02/55e6b187f0810.jpg', '1', '内衣专题', '#FAF9F5');
DROP TABLE IF EXISTS `ke_ucenter_admin`;
CREATE TABLE `ke_ucenter_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '管理员ID',
  `member_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理员用户ID',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '管理员状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='管理员表';

DROP TABLE IF EXISTS `ke_ucenter_app`;
CREATE TABLE `ke_ucenter_app` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '应用ID',
  `title` varchar(30) NOT NULL COMMENT '应用名称',
  `url` varchar(100) NOT NULL COMMENT '应用URL',
  `ip` char(15) NOT NULL DEFAULT '' COMMENT '应用IP',
  `auth_key` varchar(100) NOT NULL DEFAULT '' COMMENT '加密KEY',
  `sys_login` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '同步登陆',
  `allow_ip` varchar(255) NOT NULL DEFAULT '' COMMENT '允许访问的IP',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '应用状态',
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='应用表';

DROP TABLE IF EXISTS `ke_ucenter_member`;
CREATE TABLE `ke_ucenter_member` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `username` char(16) NOT NULL COMMENT '用户名',
  `password` char(32) NOT NULL COMMENT '密码',
  `email` char(32) NOT NULL COMMENT '用户邮箱',
  `mobile` char(15) NOT NULL DEFAULT '' COMMENT '用户手机',
  `reg_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '注册时间',
  `reg_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '注册IP',
  `last_login_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  `last_login_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后登录IP',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(4) DEFAULT '0' COMMENT '用户状态',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='用户表';

DROP TABLE IF EXISTS `ke_ucenter_setting`;
CREATE TABLE `ke_ucenter_setting` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '设置ID',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '配置类型（1-用户配置）',
  `value` text NOT NULL COMMENT '配置数据',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='设置表';


DROP TABLE IF EXISTS `ke_url`;
CREATE TABLE `ke_url` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '链接唯一标识',
  `url` char(255) NOT NULL DEFAULT '' COMMENT '链接地址',
  `short` char(100) NOT NULL DEFAULT '' COMMENT '短网址',
  `status` tinyint(2) NOT NULL DEFAULT '2' COMMENT '状态',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_url` (`url`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='链接表';

DROP TABLE IF EXISTS `ke_userdata`;
CREATE TABLE `ke_userdata` (
  `uid` int(10) unsigned NOT NULL COMMENT '用户id',
  `type` tinyint(3) unsigned NOT NULL COMMENT '类型标识',
  `target_id` int(10) unsigned NOT NULL COMMENT '目标id',
  UNIQUE KEY `uid` (`uid`,`type`,`target_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

